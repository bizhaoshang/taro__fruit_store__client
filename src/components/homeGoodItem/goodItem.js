import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text, Button } from '@tarojs/components'
import {connect} from "@tarojs/redux";
import {IMAGE_HOST} from '../../constants/server'
import { asyncAddCart } from '../../api/cart'
import {asyncLoadCart, resetCart, setCartBadge} from '../../actions/cart'
import './goodItem.styl'
import cartPng from '../../assets/images/icons/cart.png'
import groupPng from '../../assets/images/icons/group.png'

@connect(({ shop, user }) => ({
  shop,
  user
}), (dispatch) => ({
    // 加载购物车数据
    onSetCartBadge () {
      dispatch(setCartBadge())
    },
    // 加载购物车数据
    onAsyncLoadCart () {
      dispatch(asyncLoadCart())
    },
    // 重置购物车
    onResetCart () {
      dispatch(resetCart())
    },
})
)
class GoodItem extends Component {

  constructor () {
    super(...arguments)
    this.state = this.initialState
  }

  get initialState() {
    return {
    };
  }

  // 加入购物车
  addCart () {
    const {goodId, isGroup, host} = this.props
    console.log('宿主环境 host: ' , host)
    if (isGroup) {
      return Taro.navigateTo({
        url: `/pages/good/index?id=${goodId}&from=list`
      })
    }
    const {token} = this.props.user
    const {shop_id} = this.props.shop
    if (!goodId || !token || !shop_id) return
    asyncAddCart(token, shop_id, goodId, 1).then(({status}) => {
      Taro.showToast({
        title: status ? '添加成功' : '添加失败',
        icon: 'none'
      })
      if (host === 'cart') {
        console.log('更新购物车')
        this.props.onResetCart() // 重置购物车
        this.props.onAsyncLoadCart() // 更新购物车
      }
      this.props.onSetCartBadge()
    })
  }

  handleItemClick() {
    const {goodId, isGroup} = this.props
    if (isGroup) {
      Taro.navigateTo({
        url: `/pages/good/index?id=${goodId}&from=home`
      })
    }
  }

  $setShareImageUrl = () => {
    const {thumb} = this.props
    let src = null
    if (thumb) {
      src = thumb.indexOf('http') === 0 ? thumb : (IMAGE_HOST + thumb)
    }
    return src
  }

  componentWillReceiveProps (nextProps) {
    console.log(this.props, nextProps)
  }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    const {thumb, name, sales, price, tags, isGroup, spec_name, weight} = this.props
    console.log(spec_name, weight)
    // 标签
    const renderTags = tags.map((item, index) => {
      return (
        <View
          className='good-tag'
          key={index}
        >
          {item.name}
        </View>
      )
    })

    return (
      <View className='at-col good-item'  onClick={this.handleItemClick.bind(this)}>
        <View className='good-item-thumb'>
          <Image
            className='good-item-thumb__img'
            style='background: #fff;'
            src={thumb.indexOf('http') === 0 ? thumb : (IMAGE_HOST + thumb)}
          />
        </View>
        <View className='good-item-info'>
          <View className='good-name'>
            <Text>{name}</Text>
          </View>
          {/*标签*/}
          <View className='good-tags'>
            {renderTags}
          </View>
          <View className='good-sales'>
            <Text>{weight}g/{spec_name}</Text>
          </View>
          <View className='good-price'>
            <Text>{parseFloat(price / 100).toFixed(2)}</Text>
          </View>
        </View>
        <View className='good-item-cart' onClick={this.addCart.bind(this)}>
          <Image src={isGroup ? groupPng : cartPng} className='good-item-cart__icon' />
        </View>
      </View>
    )
  }
}

GoodItem.defaultProps = {
  goodId: null, // 商品id
  thumb: '', // 封面
  isGroup: false, // 拼购商品 (图标不同，可以打开详情)
  name: '', // 商品名称
  sales: 0, // 销量
  price: 0, // 价格
  host: 'home' // 宿主环境 是 首页 还是 购物车页。购物车页要通知购物车更新列表。
}

export default GoodItem
