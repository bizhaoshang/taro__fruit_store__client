import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'
import NumberInput  from '../NumberInput/index'
import './index.styl'
import checkPng from '../../assets/images/cart/checked.png'
import uncheckPng from '../../assets/images/cart/uncheck.png'
import noThumb from '../../assets/images/cart/no-thumb.png'
import {IMAGE_HOST} from '../../constants/server'

class CartGoodItem extends Component {

    config = {
    navigationBarTitleText: '首页'
  }

  handleChange () {
      console.log(this.props.onChange())
  }
  componentWillReceiveProps (nextProps) {
    console.log(this.props, nextProps)
  }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
      const {goodId, shop_id, thumb, name, price, tags,
        spec_name,
        weight,
        original_price, product_num, selected, onChange, onSelect, disable} = this.props
      // 标签
      const renderTags = tags.map((item, index) => {
        return (
          <View
            className='good-tag'
            key={index}
          >
            {item.name}
          </View>
        )
      })
    return (
      <View className='at-col good-item' onClick={onSelect.bind(this, goodId, shop_id)}>
        <View className='good-item-inner'>
          <View className='good-item-thumb'>
            <Image
              className='good-item-thumb__img'
              style='background: #fff;'
              src={thumb ? (thumb.indexOf('http') === 0 ? thumb : (IMAGE_HOST + thumb)) : noThumb}
            />
          </View>
          <View className='good-item-info'>
            {/* 商品名称 */}
            <View className='good-name'>
              <Text>{name}</Text>
            </View>
            {/* 商品规格 */}
            <View className="good-spec">
              <Text>{weight} g / {spec_name}</Text>
            </View>
            {/*标签*/}
            <View className='good-tags'>
              {renderTags}
            </View>
            {/* 价格*/}
            <View className='good-price'>
              <View className='good-price-left'>
                <Text className='now-price'>{parseFloat(price / 100).toFixed(2)}</Text>
                {original_price && <Text className='origin-price'>{original_price / 100}</Text>}
              </View>
              {/* 非本店商品不可修改数量 */}
              {disable && <Text className='good-num'>x {product_num}</Text>}
              {/*本店商品可以修改数量*/}
              {!disable && <NumberInput
                min={1}
                max={100}
                step={1}
                width={200}
                value={product_num}
                onChange={onChange.bind(this, goodId, shop_id)}
              />}
            </View>
          </View>
        </View>
        {!disable && <View className='checkbox'>
          <Image src={selected ? checkPng : uncheckPng} className='checkbox-img' />
        </View> }
      </View>
    )
  }
}

CartGoodItem.defaultProps = {
  goodId: null, // 商品id
  shop_id: null, // 店铺id
  isGroup: false, // 拼购商品 (图标不同，可以打开详情)
  tags: [], // 标签 ,
  original_price: 0, // 原价
  thumb: '', // 封面
  name: '', // 商品名称
  weight: '', //重量
  spec_name: '',//规格
  sales: 0, // 销量
  price: 0, // 价格
  product_num: 1, // 数量
  selected: false, // 是否被选中
  disable: false, // 是否是购物车中不可结算商品 或者是结算页了，都是只能看不能操作了
}

export default CartGoodItem
