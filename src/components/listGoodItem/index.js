import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'
import { AtTag, AtRate } from 'taro-ui'
import {connect} from "@tarojs/redux";

import './index.styl'
import cartPng from '../../assets/images/icons/cart.png'
import groupPng from '../../assets/images/icons/group.png'
import {IMAGE_HOST} from "../../constants/server";
import {asyncAddCart} from "../../api/cart";
import { setCartNum, setCartBadge } from '../../actions/cart'

@connect(({ shop, user, cart }) => ({
  shop,
  user,
  cart
}), (dispatch) => ({
  // 设置购物车数量
  onSetCartNum (num) {
    dispatch(setCartNum(num))
  },
  onSetCartBadge() {
    dispatch(setCartBadge())
  }
}))
class ListGoodItem extends Component {

  constructor () {
    super(...arguments)
    this.state = {
    }
  }

  config = {
    usingComponents: {
      'poster': '../poster/poster/index',
    }
  }

  // 点击商品 如果是拼购商品 进入详情
  handleItemClick() {
    const {goodId, isGroup} = this.props
    if (isGroup) {
      Taro.navigateTo({
        url: `/pages/good/index?id=${goodId}&from=list`
      })
    }
  }

  thumbClick (e) {
    e.preventDefault()
    e.stopPropagation()
    const {name, spec_name, thumb, origin_price, price, sales, weight ,goodId, tags, shopAdd} = this.props
    const shareitem = {name, spec_name, thumb, origin_price, price, sales, weight, goodId, tags, shopAdd}
    this.props.onThumbClick(shareitem, e)
  }

  // 加入购物车
  addCart () {
    const {goodId, isGroup} = this.props
    if (isGroup) {
      return Taro.navigateTo({
        url: `/pages/good/index?id=${goodId}&from=list`
      })
    }
    const {token} = this.props.user
    const {shop_id} = this.props.shop
    if (!goodId || !token || !shop_id) return
    asyncAddCart(token, shop_id, goodId, 1).then(({status}) => {
      Taro.showToast({
        title: status ? '添加成功' : '添加失败',
        icon: 'none'
      })
      if (status) {
        this.props.onSetCartBadge()
      }
    })
  }

  componentWillUnmount () { }

  componentDidMount () {}
  componentDidHide () { }

  render () {
    const { tags, star, offer, original_price, isGroup, name, spec_name, weight, thumb, origin_price, price, sales} = this.props

    // 标签
      const renderTags = tags.map((item, index) => {
        return (
          <View
            className='good-tag'
            key={index}
          >
            {item.name}
          </View>
        )
      })
    // 优惠
    const renderOffer = offer.map((item, index) => {
      return (
        <View
          className='good-offer-tag'
          key={index}
        >
          {item}
        </View>
      )
    })
    return (
      <View className='at-col good-item' onClick={this.handleItemClick.bind(this)}>
        <View className='good-item-thumb' onClick={this.thumbClick.bind(this)}>
          <Image
            className='good-item-thumb__img'
            style='background: #fff;'
            src={thumb.indexOf('http') === 0 ? thumb : (IMAGE_HOST + thumb)}
          />
        </View>
        <View className='good-item-info'>
          {/* 商品名称 */}
          <View className='good-name'>
            <Text>{name}</Text>
          </View>
          {/*标签*/}
          <View className='good-tags'>
            {renderTags}
          </View>
          {/*评价*/}
          <View className='good-star'>
            <Text className='label'>评价</Text>
            <View className='inline-block'>
              <AtRate value={star} size='10' />
            </View>
          </View>
          {/*规格*/}
          <View className='good-sales'>
            <Text>{weight}g/{spec_name}</Text>
          </View>
          {/*优惠*/}
          <View className='good-offer'>
            {renderOffer}
          </View>
          {/* 价格*/}
          <View className='good-price'>
            <Text className='now-price'>{parseFloat(price / 100).toFixed(2)}</Text>
            {original_price && <Text className='origin-price'>{parseFloat(original_price / 100).toFixed(2)}</Text>}
          </View>
        </View>
        <View className='good-item-cart' onClick={this.addCart.bind(this)}>
          <Image src={isGroup ? groupPng : cartPng} className='good-item-cart__icon' />
        </View>
      </View>
    )
  }
}

ListGoodItem.defaultProps = {
  goodId: null, // 商品id
  isGroup: false, // 拼购商品 (图标不同，可以打开详情)
  tags: [], // 标签 ,
  star: 5, // 评分
  offer: [], // 优惠信息
  original_price: 0, // 原价
  thumb: '', // 封面
  name: '', // 商品名称
  sales: 0, // 销量
  price: 0, // 价格
  weight: '--', // 重量
  shopAdd:{} //位置
}

export default ListGoodItem
