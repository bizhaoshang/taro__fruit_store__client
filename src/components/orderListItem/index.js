import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'
import {connect} from "@tarojs/redux";
import {AtAvatar} from "taro-ui";
import { asyncShop, updateShop } from '../../actions/shop'

import './index.styl'
import {IMAGE_HOST} from "../../constants/server";
import {asyncOrderService} from '../../api/order'

@connect(({ user,shop }) => ({
  user,shop
}), (dispatch) => ({

  // 加载店铺信息
  onAsyncShop() {
    dispatch(asyncShop())
  }
}))
class OrderListItem extends Component {

  constructor () {
    super(...arguments)
    this.state = {}
  }
  config = {
    usingComponents: {
      'count-down': '../count-down/count-down'
    }
  }

  componentWillReceiveProps (nextProps) {
    console.log(this.props, nextProps)
  }

  // 扫码签收
  finishOrder () {
    const {orderId} = this.props
    this.props.onFinishOrder(orderId)
  }

  // 拍十秒

  // 预览商品实拍图
  preView (photo) {
    Taro.previewImage({
      current: photo,
      urls: [photo]
    })
  }

  // 售后升级
  orderService () {
    const { token } = this.props.user
    const { code } = this.props
    Taro.showModal({
      title: '请选择你需要的售后服务',
      confirmText: '售后升级',
      cancelText: '联系商家'
    })
      .then(res => {
        console.log(res.confirm, res.cancel)
        if (res.cancel) {
          Taro.makePhoneCall({
            phoneNumber: this.props.shop.tel // 仅为示例，并非真实的电话号码
          })
        }
        if (res.confirm) {
          asyncOrderService(token, code).then(({ status, msg }) => {
            Taro.showToast({
              title: status ? '请等待平台客服人员与您沟通' : msg,
              icon: 'none'
            })
          })
        }
      })

  }


  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    // TODO 支付金额没有明确
    const {orderId, shopId, user_status, order_status, code, products, address,
      created_at, // 创建时间
      completed_at, // 完成时间
      total_money, // 订单实付金额
      total_amount, // 订单总额
      shipping_fee,//配送费
      tenseconds, // 能否拍十秒
      group, // 团
    } = this.props
    // 商品实拍
    const pics = products.map(prod => {
      return prod.pic
    }).filter(pic => !!pic)


    // 商品列表
    const renderProducts = products.map((product, index) => {
      let imgsrc = 'https://img.icons8.com/dusk/64/000000/raspberry.png'
      if (product.product_thumb) {
        if (product.product_thumb.startsWith('http')) {
          imgsrc = product.product_thumb
        } else {
          imgsrc = IMAGE_HOST + product.product_thumb
        }
      }
      return (<View key={index} className='order-item-product'>
        <Image
          className='order-item-product__thumb'
          style='background: #fff;'
          src={imgsrc}
        />
        <View className='order-item-product__info'>
          <View className='order-item-product__name'>{product.product_name}</View>
          <View className='order-item-product__name'>{product.weight}g/{product.spec_name}</View>
          <View className='order-item-product__price'>￥{parseFloat(product.product_sale_price / 100).toFixed(2)}</View>
        </View>
        <View className='order-item-product__action'>
        </View>
        <View className='order-item-product__num'>
          x{product.product_num}
        </View>
      </View>)
    })

    // 商品实拍
    const renderPhotos = pics.map((photo, index) => {
      return (<Image
        className='order-item-photos__photo'
        style='background: #fff;'
        src={photo} key={index}
        onClick={this.preView.bind(this, photo)}
      />)
    })

    // 团成员列表
    const groupMembers = ((group && group.users) || []).map((member, index) => {
      return <View key={index} className='order-group-member'>
        <AtAvatar circle size='small' image={member.avatar} className='order-group-avatar' />
        <Text className='order-group-nick'>{member.nickname}</Text>
      </View>

    })
    return (
      <View className='order-item'>
        {/*头部*/}
        <View className='order-item-header'>
          <Text className='order-item-header__code'>{code}</Text>
          <Text  className='order-item-header__status'>{user_status}</Text>
        </View>
        {/*团信息*/}
        {group && <View className='order-group'>
          <View className='order-group-members'>
            {groupMembers}
            {group && (group.need_num - group.user_num > 0) && <View  className='order-group-member'>
              <AtAvatar circle size='small' text='?' className='order-group-avatar' />
              <Text className='order-group-nick'>还差<Text className='order-group-left'>{group.need_num - group.user_num}</Text>人</Text>
            </View>}
          </View>
          <View className='order-group-timeout'>
            {/* 还未完成拼团时*/}
            {group && (group.need_num - group.user_num > 0) && <View className='order-group-deadline'>
              <View className='order-group-left-big'>还差<Text className='order-group-left'>{group.need_num - group.user_num}</Text>人拼成</View>
              <count-down initDuration={group.minutes * 60} />
            </View>}
            {group && (group.need_num - group.user_num <= 0) && <View className='order-group-deadline'>
              <View className='order-group-left-big'>拼团成功</View>
            </View>}
            <View className='order-group-share' onClick={this.props.onGroupShare.bind(this,orderId,group.need_num - group.user_num,shopId)}>{group.need_num - group.user_num > 0 ? '邀请好友' : '分享'}</View>
          </View>
        </View>}
        {/*商品信息*/}
        <View className='order-item-products'>
          {renderProducts}
        </View>

        {(order_status === 3) && <View className='order-item-photos'>
          <View className='order-item-photos__header'>
            商品实拍
          </View>
          <View className='order-item-photos__body'>
            {renderPhotos}
          </View>
        </View>}

        {/* 商品实拍 */}
        {(pics.length > 0) && <View className='order-item-photos'>
          <View className='order-item-photos__header'>
            商品实拍
          </View>
          <View className='order-item-photos__body'>
            {renderPhotos}
          </View>
        </View>}
        {/*收货人信息*/}
        <View className='order-item-user'>
          <View className='order-item-user__item'>
            <View className='order-item-user__label'>
              收货地址:
            </View>
            <View className='order-item-user__value'>
              {address ? (address.address + '' + address.truename + '（' + address.gender + '）') : '到店自取'}
            </View>
          </View>
          <View className='order-item-user__item'>
            <View className='order-item-user__label'>
              联系电话:
            </View>
            <View className='order-item-user__value'>
              <Text>{address ? address.phone : '到店自取'}</Text>
            </View>
          </View>
        </View>
        {/*时间信息*/}
        <View className='order-item-date'>
          <View className='order-item-date__item'>
            <View className='order-item-date__label'>
              创建时间:
            </View>
            <View className='order-item-date__value'>
              {created_at}
            </View>
          </View>
          <View className='order-item-date__item'>
            <View className='order-item-date__label'>
              收货时间:
            </View>
            <View className='order-item-date__value'>
              {completed_at || ''}
            </View>
          </View>
        </View>
        {/**/}
        {/*付款信息*/}
        <View className='order-item-pay'>
          <View className='order-item-pay__item'>
            <View className='order-item-pay__label'>
              商品总价:
            </View>
            <View className='order-item-pay__value'>
              {parseFloat(total_amount / 100).toFixed(2)}
            </View>
          </View>
          <View className='order-item-pay__item'>
            <View className='order-item-pay__label'>
              优惠金额: {parseFloat((total_amount+shipping_fee - total_money) / 100).toFixed(2)}
            </View>
            <View className='order-item-pay__value'>
             
            </View>
          </View>
          <View className='order-item-pay__item'>
            <View className='order-item-pay__label'>
              商品应付:
            </View>
            <View className='order-item-pay__value order-item-pay__value-big'>
              {parseFloat(total_money / 100).toFixed(2)}
            </View>
          </View>
          <View className='order-item-pay__item'>
            <View className='order-item-pay__label'>
              配送费:
            </View>
            <View className='order-item-pay__value'>
              {parseFloat(shipping_fee / 100).toFixed(2)}
            </View>
          </View>
        </View>
        {/*订单操作*/}
        <View className='order-item-actions'>
          {((order_status > 0 && order_status < 6 )|| order_status==11) && <View className='order-item-action' onClick={this.props.onOrderCancel.bind(this, code, orderId)}>
            取消订单
          </View>}
          {order_status === 5 && <View className='order-item-action'  onClick={this.finishOrder.bind(this)}>
            出示收货码
          </View>}
          {/*拍十秒*/}
          {(order_status === 100 || order_status === 200)  && tenseconds === 0 && <View className='order-item-minute' onClick={this.props.onMinute.bind(this, orderId,shopId, code)} />}

          {order_status === 100 && <View className='order-item-action'  onClick={this.props.onPointOrder.bind(this, orderId)}>
            评价订单
          </View>}
          {/*售后升级*/}
          {(order_status === -4 || order_status=== 100 || order_status===200) && <View className='order-item-action' onClick={this.orderService.bind(this, code, orderId)}>
            售后升级
          </View>}
        </View>
      </View>
    )
  }
}

OrderListItem.defaultProps = {
  orderId: null, // 订单id
  order_status: null, // 订单状态
  code: null, // 订单编号
  user_status: '', // 客户看到的 订单状态
  products: [], // 物品
  address: '', // 收货地址
  phone: '', // 收货人电话
  created_at: '', //  创建时间
  cameraContext: null, // 相机组件
  total_money: '', // 订单实付金额
  total_amount: '', // 订单总额
  group: null, // 团信息
}

export default OrderListItem
