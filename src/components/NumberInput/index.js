import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import './index.styl'

class NumberInput extends Component {
  // 增加
  onAdd (e) {
    e.preventDefault()
    e.stopPropagation()
    const {max, value} = this.props
    if (max === value) return
    this.props.onChange(value + 1)
  }

  // 减少
  onReduce (e) {
    e.preventDefault()
    e.stopPropagation()
    const {min, value} = this.props
    if (min === value) return
    this.props.onChange(value - 1)
  }

  render () {
    const {min, max, value} = this.props
    return (<View className='number-input'>
      <Text onClick={this.onReduce.bind(this)} className={min >= value ? 'shopfont shop-reduce disable' : 'shopfont shop-arrow shop-reduce'} />
      <Text className='value'>{value}</Text>
      <Text onClick={this.onAdd.bind(this)} className={max <= value ? 'shopfont shop-add disable' : 'shopfont shop-add'} />
    </View>)
  }
}

NumberInput.defaultProps = {
  min: 0, // 最小值
  max: 100, // 最大值
  step: 1, // 步进
  value: 0 // 默认值
}

export default NumberInput
