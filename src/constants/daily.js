export const DAIYL_LOAD = 'DAIYL_LOAD'
export const DAIYL_LOADING = 'DAIYL_LOADING'
export const DAIYL_LOADEND = 'DAIYL_LOADEND'
export const DAIYL_NOMORE = 'DAIYL_NOMORE'
export const DAIYL_RESET = 'DAIYL_RESET'
export const DAIYL_RESET_LIST = 'DAIYL_RESET_LIST' // 对列表重置，用于搜索前
export const DAIYL_HOME_LIST = 'DAIYL_HOME_LIST'
export const DAIYL_KEYWORD = 'DAIYL_KEYWORD' // 设置搜索关键词

