export const LOAD_CART = 'LOAD_CART'
export const CART_RESET = 'CART_RESET' // 重置购物车
export const SET_CART_UNPAY = 'SET_CART_UNPAY' // 设置待支付订单
export const SET_CART_NUM = 'SET_CART_NUM'
export const SET_CART_OFFER = 'SET_CART_OFFER' // 购物车推荐商品


