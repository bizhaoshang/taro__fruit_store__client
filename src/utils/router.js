import Taro from '@tarojs/taro'

const PAGE_LEVEL_LIMIT = 10
// 处理微信跳转超过10层
export function jumpUrl (url, options = {} ) {
  const pages = Taro.getCurrentPages()
  let method = options.method || 'navigateTo'
  if (url && typeof url === 'string') {
    if (method === 'navigateTo' && pages.length >= PAGE_LEVEL_LIMIT - 3) {
      method = 'redirectTo'
    }

    if (method === 'navigateToByForce') {
      method = 'navigateTo'
    }

    if (method === 'navigateTo' && pages.length == PAGE_LEVEL_LIMIT) {
      method = 'redirectTo'
    }

    Taro[method]({
      url
    })
  }
}
