import { ADDRESS_LIST_LOAD, ADDRESS_SELECT } from '../constants/address'

const INITIAL_STATE = {
  addressList: [], // 地址列表
  select_id: null // 选中地址
}

export default function address (state = INITIAL_STATE, action) {
  switch (action.type) {
    // 加载地址列表
    case ADDRESS_LIST_LOAD:
      return {
        ...state,
        addressList: action.addressList
      }
    // 选中地址
    case ADDRESS_SELECT:
      return {
        ...state,
        select_id: action.select_id
      }
     default:
       return state
  }
}
