import Taro from '@tarojs/taro'
import { UPDATE } from '../constants/shop'

// 从存储中读取 shop_id
const shop_id = Taro.getStorageSync('shop_id')

const INITIAL_STATE = {
  shop_id, // 店铺id
  name: '', // 店铺名称
  logo: '', // 店铺logo
  tel: '', // 店铺电话
  notice: '', // 店铺公告
  min_orderamount: 0 // 店铺包邮金额 不满足无法下单
}

export default function shop (state = INITIAL_STATE, action) {
  switch (action.type) {
    case UPDATE:
      return {
        ...state,
        ...action.data
      }
     default:
       return state
  }
}
