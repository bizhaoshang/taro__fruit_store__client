import { ORDER_LOAD, ORDER_RESET, ORDER_REFRESH } from '../constants/order'


const INITIAL_STATE = {
  // 需要刷新,用于从订单页到别的页再返回的情况
  need_refresh: false,
  // 全部列表
  all: [],
  all_page: 1,
  all_nomore: false,
  // 待付款
  wait_pay: [],
  wait_pay_page: 1,
  wait_pay_nomore: false,
  // 待收货
  wait_receive: [],
  wait_receive_page: 1,
  wait_receive_nomore: false,
  // 已完成
  done: [],
  done_page: 1,
  done_nomore: false,
  // 待评价
  wait_point: [],
  wait_point_page: 1,
  wait_point_nomore: false,
}

export default function order (state = INITIAL_STATE, action) {
  switch (action.type) {
    case ORDER_LOAD:
      return {
        ...state,
        ...action.data
      }
    case ORDER_RESET:
      return {
        ...state,
        ...action.data
      }
    case ORDER_REFRESH:
      return {
        ...state,
        need_refresh: action.refresh
      }
     default:
       return state
  }
}
