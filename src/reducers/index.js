import { combineReducers } from 'redux'
import shop from './shop'
import daily from './daily'
import groupBuy from './groupBuy'
import good from './good'
import user from './user'
import address from './address'
import coupon from './coupon'
import cart from './cart'
import order from './order'
import reduction from './reduction'
import voucher from './voucher'

export default combineReducers({
  shop,
  daily,
  groupBuy,
  good,
  user,
  address,
  coupon,
  cart,
  order,
  reduction,
  voucher
})
