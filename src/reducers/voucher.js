import { LOAD_DCOUPON, DCOUPON_RESET, DCOUPON_SELECT } from '../constants/voucher'
const INITIAL_STATEB = {
  data: [], // 优惠券列表
  page: 1, // 当前页
  nomore: false, // 没有更多了
  loading: false, // 正在加载中 
  select_voucher_id: null, // 选中券
}

export default function coupon (state = INITIAL_STATEB, action) {
  switch (action.type) {
    case LOAD_DCOUPON:
      return {
        ...state,
        ...action.data
      }
    // 选中券
    case DCOUPON_SELECT:
      return {
        ...state,
        select_voucher_id: action.select_id
      }
    // TODO 重置 不要重复定义
    case DCOUPON_RESET:
      return {
        ...state,
        ...action.data,
        select_id: null,
      }
     default:
       return state
  }
}
