import Taro from '@tarojs/taro'
import { CouponStare } from '../constants/reduction'

// 从存储中读取 shop_id

const INITIAL_STATE = {
  data: [] // 全部列表
}

export default function reduction (state = INITIAL_STATE, action) {
  switch (action.type) {
    case CouponStare:
      return {
        ...state,
        ...action.data
      }
     default:
       return state
  }
}
 