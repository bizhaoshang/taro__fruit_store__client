import { GROUP_BUY_LOAD, GROUP_BUY_LOADING, GROUP_BUY_RESET, GROUP_BUY_HOME_LIST, GROUP_BUY_KEYWORD, GROUP_BUY_RESET_LIST } from '../constants/groupBuy'

// 社区拼购数据
const INITIAL_STATE = () => {
  return {
    page: 1, // 当前页
    search: '', //搜索关键字
    nomore: false, // 没有更多了
    loading: false, // 正在加载中
    homeList: [], // 首页列表
    list: [] // 全部列表
  }
}

export default function groupbuy (state = INITIAL_STATE(), action) {
  switch (action.type) {
    // 重置
    case GROUP_BUY_RESET:
      return INITIAL_STATE()
    // 重置列表
    case GROUP_BUY_RESET_LIST: {
      return {
        ...state,
        list: [],
        page: 1, // 当前页
        loading: false, // 正在加载中
        nomore: false, // 没有更多了
      }
    }
    // 首页数据
    case GROUP_BUY_HOME_LIST:
      return {
        ...state,
        homeList: action.homeList
      }
    // 列表数据
    case GROUP_BUY_LOAD:
      return {
        ...state,
        list: [...state.list, ...action.data.list],
        page: action.data.page,
        nomore: action.data.nomore
      }
    // 设置关键词
    case GROUP_BUY_KEYWORD:
      return {
        ...state,
        search: action.data
      }
    // 加载中
    case GROUP_BUY_LOADING:
      return {
        ...state,
        loading: action.loading
      }
    default:
      return state
  }
}
