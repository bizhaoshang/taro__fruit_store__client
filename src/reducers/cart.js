import { LOAD_CART, CART_RESET, SET_CART_UNPAY, SET_CART_NUM, SET_CART_OFFER } from '../constants/cart'

const INITIAL_STATE = () => {
  return {
    cart_num: 0, // 购物车数量
    list: [], // 购物车列表
    disList: [], // 不可以结算的
    offerList: [], // 购物车推荐商品, 购物车为空时展示
    page: 1, // 页码
    loading: false, // 加载中
    nomore: false, // 没有更多
    deliveryMethod: 'shop', // 配送方式
    unpay: { // 待支付订单
      appId: null, // 支付参数
      nonceStr: null, // 支付参数
      order_code: null, // 支付参数
      apackage: null, // 支付参数 package 因占用了关键字 这里做了重命名
      paySign: null, // 支付参数
      signType: null, // 支付参数
      timeStamp: null, // 支付参数
      products: [], // 商品列表
      total: 0, // 商品总价
      real_total: 0, // 实付款
      select_coupon_id: null, // 未支付订单选中的券id
      select_voucher_id: null, // 未支付订单选中的代金券id
      select_address_id: null, // 未支付订单选中的地址id
    }
  }
}

export default function cart (state = INITIAL_STATE(), action) {
  switch (action.type) {
    // 加载购物车列表
    case LOAD_CART:
      return {
        ...state,
        ...action.data
      }
    // 设置待支付订单
    case SET_CART_UNPAY:
      return {
        ...state,
        unpay: {
          ...action.data
        }
      }
    // 设置购物车数量
    case SET_CART_NUM:
      return {
        ...state,
        cart_num: action.num
      }
    // 设置购物车推荐商品
    case SET_CART_OFFER:
      return {
        ...state,
        offerList: action.offerList
      }
      // 重置购物车列表
    case CART_RESET:
      return Object.assign({}, INITIAL_STATE(), {offerList: state.offerList})

    default:
      return state
  }
}
