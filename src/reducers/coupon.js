import { LOAD_COUPON, COUPON_RESET, COUPON_SELECT } from '../constants/coupon'

const INITIAL_STATE = {
  list: [], // 优惠券列表
  page: 1, // 当前页
  nomore: false, // 没有更多了
  loading: false, // 正在加载中
  select_coupon_id: null, // 选中券
}

export default function coupon (state = INITIAL_STATE, action) {
  switch (action.type) {
    case LOAD_COUPON:
      return {
        ...state,
        ...action.data
      }
    // 选中券
    case COUPON_SELECT:
      return {
        ...state,
        select_coupon_id: action.select_id
      }
    // TODO 重置 不要重复定义
    case COUPON_RESET:
      return {
        ...state,
        list: [],
        page: 1, // 当前页
        loading: false, // 正在加载中
        nomore: false, // 没有更多了
        select_id: null
      }
     default:
       return state
  }
}
