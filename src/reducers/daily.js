import { DAIYL_LOAD, DAIYL_LOADING, DAIYL_RESET, DAIYL_HOME_LIST, DAIYL_RESET_LIST, DAIYL_KEYWORD } from '../constants/daily'

// 每日鲜果数据
const INITIAL_STATE = () => {
  return {
    page: 1, // 当前页
    search: '', //搜索关键字
    nomore: false, // 没有更多了
    loading: false, // 正在加载中
    homeList: [], // 首页列表
    list: [] // 全部列表
  }
}

export default function daily (state = INITIAL_STATE(), action) {
  switch (action.type) {
    // 重置
    case DAIYL_RESET:
      return INITIAL_STATE()
    // 重置列表
    case DAIYL_RESET_LIST: {
      return {
        ...state,
        list: [],
        page: 1, // 当前页
        loading: false, // 正在加载中
        nomore: false, // 没有更多了
      }
    }
    // 首页数据
    case DAIYL_HOME_LIST:
      return {
        ...state,
        homeList: action.homeList
      }
    // 列表数据
    case DAIYL_LOAD:
      return {
        ...state,
        list: [...state.list, ...action.data.list],
        page: action.data.page,
        nomore: action.data.nomore
      }
    // 搜索关键词
    case DAIYL_KEYWORD:
      return {
        ...state,
        search: action.data
      }
    case DAIYL_LOADING:
      return {
        ...state,
        loading: action.loading
      }
    default:
      return state
  }
}
