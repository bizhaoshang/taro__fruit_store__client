import { GOOD_LOAD, GOOD_BASIC, GOOD_GROUP_LIST, GOOD_DETAIL } from '../constants/good'
// 商品详情数据

const INITIAL_STATE = {
  goodId: null, // 商品id
  banner: [], // 商品的Banner
  name: '', // 商品名称
  sales: 0, // 商品效率
  describe: '', // 商品月销量
  price: 900, // 商品价格
  detail: '<div></div>', // 商品富文本详情
  group_list: [], // 拼团的团列表
  group_list_id: null, // 拼团的团列表id
  group_total: 0, // 拼团的人总数
  group_num: 0, // X人成团 ，x人数
  weight: '--' , // 重量
  user_group:[]
}

export default function good (state = INITIAL_STATE, action) {
  switch (action.type) {
    case GOOD_BASIC:
      return {
        ...state,
        ...action.data
      }
    case GOOD_LOAD:
      return {
        ...state,
        ...action.data
      }
    // 商品的拼购信息
    case GOOD_GROUP_LIST:
      return {
        ...state,
        ...action.data
      }
    // 二维码商品的拼购详情
    case GOOD_DETAIL:
      return {
        ...state,
        ...action.data
      }
    default:
      return state
  }
}
