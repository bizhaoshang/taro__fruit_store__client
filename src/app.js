import '@tarojs/async-await'
import Taro, { Component } from '@tarojs/taro'
import { Provider } from '@tarojs/redux'

import Index from './pages/index'

import configStore from './store'

import './app.styl'

if (process.env.TARO_ENV === "weapp") {
  require("taro-ui/dist/weapp/css/index.css")
} else if (process.env.TARO_ENV === "h5") {
  require("taro-ui/dist/h5/css/index.css")
}
const store = configStore()

class App extends Component {

  config = {
    pages: [
      'pages/index/index', // 首页
      'pages/cart/index', // 购物车
      'pages/cart/settle', // 购物车第二步 提交订单
      'pages/cart/unpay', // 购物车第三步 待支付
      'pages/address/index', // 地址管理
      'pages/address/edit', // 地址管理 - 编辑
      'pages/coupon/index', // 我的卡券
      'pages/user/index', // 个人中心
      'pages/user/money', // 个人中心 - 余额
      'pages/user/privilege', // 个人中心 - 我的特权
      'pages/good/index', // 商品详情
      'pages/daily/index', // 每日鲜果
      'pages/groupBuy/index', // 社区拼购
      'pages/groupBuy/show', // 拼购详情
      'pages/order/index', // 我的订单
      'pages/order/point', // 我的订单 - 评价
      'pages/game/minute', // 游戏 - 拍十秒
      'pages/benefit/index', // 惠店买单
      'pages/voucher/index', // 代金单
    ],
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#FFD701',
      navigationBarTitleText: 'WeChat',
      navigationBarTextStyle: 'black',
      enablePullDownRefresh: true
    },
    tabBar: {
      color: '#7E7E7E',
      selectedColor: '#111111',
      backgroundColor: '#FFFFFF',
      borderStyle: 'black',
      list: [{
        pagePath: 'pages/index/index',
        text: '首页',
        iconPath: './assets/images/tabbar/tab-home.png',
        selectedIconPath: './assets/images/tabbar/tab-home-active.png'
      },{
        pagePath: 'pages/cart/index',
        text: '购物车',
        iconPath: './assets/images/tabbar/tab-cart.png',
        selectedIconPath: './assets/images/tabbar/tab-cart-active.png'
      },
        {
          pagePath: 'pages/user/index',
          text: '我的',
          iconPath: './assets/images/tabbar/tab-user.png',
          selectedIconPath: './assets/images/tabbar/tab-user-active.png'
        }]
    },
    "permission": {
      "scope.userLocation": {
        "desc": "你的位置信息将用于向您推荐最近的水果店"
      }
    }
  }

  componentDidMount () {}

  componentDidShow () {}

  componentDidHide () {}

  componentCatchError () {}

  componentDidCatchError () {}

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render () {
    return (
      <Provider store={store}>
        <Index />
      </Provider>
    )
  }
}

Taro.render(<App />, document.getElementById('app'))
