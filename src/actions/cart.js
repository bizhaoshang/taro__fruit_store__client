import Taro from '@tarojs/taro'
import { LOAD_CART, CART_RESET, SET_CART_UNPAY, SET_CART_NUM, SET_CART_OFFER } from '../constants/cart'
import { asyncCart, asyncReduceCart, asyncRemoveCart, asyncAddCart } from '../api/cart'
import { asyncOfferList } from '../api/good' // 加载购物车推荐商品列表

// 加载购物车
export const loadCart = (data) => {
  return {
    type: LOAD_CART,
    data
  }
}

// 加载购物车推荐商品
export const loadCartOffer = (offerList) => {
  return {
    type: SET_CART_OFFER,
    offerList
  }
}

// 重置购物车
export const resetCart = () => {
  return {
    type: CART_RESET
  }
}

// 设置待支付订单
export const setUnpay = (data) => {
  return {
    type: SET_CART_UNPAY,
    data
  }
}

// 设置待支付订单
export const setCartNum = (num) => {
  return {
    type: SET_CART_NUM,
    num
  }
}

// 设置 tabbar 购物车数量
export const setCartBadge = () => {
  console.log('设置 tabbar 购物车数量')
  return (dispatch, getState)  => {
    const { token } = getState().user
    return asyncCart(token).then(({data, status}) => {
      if (status) {
        if (data.total) {
          Taro.setTabBarBadge({
            index: 1,
            text: String(data.total)
          })
          dispatch(setCartNum(data.total))
        } else {
          Taro.removeTabBarBadge({
            index: 1
          })
        }
      }
    })
  }
}


// 异步的action 获取购物车信息
export function asyncLoadCart () {
  return (dispatch, getState)  => {
    const { token } = getState().user
    const { page, nomore } = getState().cart
    const { shop_id } = getState().shop
    if (!token || nomore) return
    const nextPage = page + 1

    return asyncCart(token).then(({data, status}) => {
      // TODO 获取失败的处理
      if (status) {
        const nomore2 = data.data.length < 10
        const list = data.data.filter(product => product.shop_id === shop_id) // 可以结算的
        const disList = data.data.filter(product => product.shop_id !== shop_id) // 不可以结算的
        console.log('setCartBadge')
        setCartBadge()
          dispatch(loadCart({
            list: list.map(product => Object.assign({}, product, {selected: true})),
            disList, // 不可结算的
            page: nextPage,
            nomore: nomore2
          }))
        if (!nomore2) {
          dispatch(asyncLoadCart())
        }
      }
    })
  }
}

// 异步 增加购物车数量
export function asyncAddCartAction (shop_id, product_id, product_num) {
  return (dispatch, getState)  => {
    const { token } = getState().user
    if (!token) return
    const list = JSON.parse(JSON.stringify(getState().cart.list))
    const index = list.findIndex(good => good.product_id === product_id)
    list[index].product_num = product_num
    dispatch(loadCart({list}))

    return asyncAddCart(token, shop_id, product_id, 1).then(({status}) => {
      if (status) {
        console.log('添加成功')
      }
    })
  }
}

// 异步 减少购物车数量
export function asyncReduceCartAction (product_id, product_num) {
  return (dispatch, getState)  => {
    const { token } = getState().user
    if (!token) return
    const list = JSON.parse(JSON.stringify(getState().cart.list))
    const index = list.findIndex(good => good.product_id === product_id)
    list[index].product_num = product_num
    dispatch(loadCart({list}))

    return asyncReduceCart(token, product_id).then(({status}) => {
      // TODO 获取失败的处理
      if (status) {
        console.log('减少成功')
      }
    })
  }
}

// 异步 删除购物车
export function asyncRemoveCartAction (cart_id) {
  return (dispatch, getState)  => {
    const { token } = getState().user
    if (!token) return
    const list = JSON.parse(JSON.stringify(getState().cart.list))
    const index = list.findIndex(good => good.id === cart_id)
    list.splice(index, 1)
    dispatch(loadCart({list}))

    return asyncRemoveCart(token, cart_id).then(({status}) => {
      // TODO 获取失败的处理
      if (status) {
        console.log('删除成功')
      }
    })
  }
}

// 异步 获取购物车推荐商品
export function asyncLoadOffer () {
  console.log('action 加载购物车推荐商品')
  return (dispatch, getState)  => {
    const { token } = getState().user
    const { shop_id } = getState().shop
    if (!token || !shop_id) return
    return asyncOfferList(token, shop_id).then(({data, status}) => {
      if (status) {
        dispatch(loadCartOffer(data))
      }
    })
  }
}
