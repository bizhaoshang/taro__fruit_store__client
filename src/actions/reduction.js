import { CouponStare } from '../constants/reduction'
import {asyncWechatCoupon} from '../api/reduction'

export const updateCoupon = (data) => {
  return {
    type: CouponStare,
    data
  }
}

// 异步的action 获取店铺信息
export function asyncWechatCouponA () {
  return (dispatch, getState)  => {
    const { token } = getState().user
    asyncWechatCoupon(token).then(({data, status}) => {
      if (status) {
          dispatch(updateCoupon({
            data
          }))
      }
    })
  }
}

