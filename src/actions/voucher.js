import { LOAD_DCOUPON, DCOUPON_RESET, DCOUPON_SELECT } from '../constants/voucher'
import { asyncUserVoucher } from '../api/voucher'


// 设置用户的优惠券
export const setUserVoucher = data => {
  return {
    type: LOAD_DCOUPON,
    data
  }
}

// 选中券
export const selectVoucher = select_id => {
  return {
    type: DCOUPON_SELECT,
    select_id
  }
}

// 置用户的优惠券
export const resetUserVoucher = data => {
  return {
    type: DCOUPON_RESET,
    data
  }
}

// 异步获取优惠券
export const asyncGetUserdVoucher = (shop_only = false) => {
  return (dispatch, getState) => {
    const { token } = getState().user
    const { page } = getState().coupon
    const { shop_id } = getState().shop
    if (!token) return
    const nextPage = page + 1

    asyncUserVoucher(token, page, shop_only ? shop_id : null).then(({data, status}) => {
      console.log(data)
      if (status) {
        dispatch(setUserVoucher({
          data: data,
          page: nextPage,
        }))
      }
    })
  }
}
