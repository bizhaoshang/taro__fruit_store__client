import { UPDATE } from '../constants/shop'
import {asyncGetShopInfo} from '../api/shop'

export const updateShop = (data) => {
  return {
    type: UPDATE,
    data
  }
}

// 异步的action 获取店铺信息
export function asyncShop () {
  return (dispatch, getState)  => {
    const { token } = getState().user
    const { shop_id } = getState().shop
    if (!token || !shop_id) return // TODO 二者不在的处理
    asyncGetShopInfo(token, shop_id).then(({data, status}) => {
      if (status) {
        const {name, notice, phone: tel, logo, min_orderamount,address,shipping_fee} = data
          dispatch(updateShop({
            name, // 店铺名称
            logo, // 店铺logo
            tel, // 店铺电话
            notice, // 店铺公告
            address, //店铺地址
            shipping_fee, //配送费
            min_orderamount: parseFloat(min_orderamount * 100) // 起送金额
          }))
      }
    })
  }
}

