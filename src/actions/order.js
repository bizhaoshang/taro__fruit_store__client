
import { ORDER_RESET, ORDER_LOAD, ORDER_REFRESH } from '../constants/order'
import { asyncOrderList } from '../api/order'

// 重置列表
export const orderReset = (list_type) => {
  return {
    type: ORDER_RESET,
    data: {
      [list_type]: [],
      [list_type + '_page']: 1,
      [list_type + '_nomore']: false
    }
  }
}

// 加载列表
export const orderLoad = (data) => {
  return {
    type: ORDER_LOAD,
    data
  }
}

// 设置需要刷新
export const orderRefresh = (bool) => {
  return {
    type: ORDER_REFRESH,
    refresh: bool
  }
}


// 异步的action 加载全部列表
export function asyncLoadOrderAllList (pageNum) {

  console.log("加载订单列表"+pageNum)
  return (dispatch, getState) => {
    console.log(getState())
    const { token } = getState().user
    const {
      all,
      all_page,
      all_nomore
    } = getState().order

    if (!token || all_nomore ) return
    
    const nextPage = all_page + 1
    asyncOrderList(token, 1,  pageNum,5).then(({data, status}) => {
      if (status) {
        dispatch(orderLoad({
          all: all.concat(data.data),
          all_page: nextPage,
          all_nomore:  data.data.length < 5
        }))
      }
    })
  }
}

// 异步的action 加载待付款列表
export function asyncLoadOrderUnpayList () {
  return (dispatch, getState) => {
    const { token } = getState().user
    const {
      wait_pay, // 待支付
      wait_pay_page,
      wait_pay_nomore,
    } = getState().order

    if (!token || wait_pay_nomore ) return

    const nextPage = wait_pay_page + 1
    asyncOrderList(token, 1,  wait_pay_page).then(({data, status}) => {
      if (status) {
        dispatch(orderLoad({
          wait_pay: wait_pay.concat(data.data),
          wait_pay_page: nextPage,
          wait_pay_nomore:  data.data.length < 5
        }))
      }
    })
  }
}

// 异步的action 加载待收货列表
export function asyncLoadOrderUnreceiveList () {
  return (dispatch, getState) => {
    const { token } = getState().user
    const {
      wait_receive, // 待收货
      wait_receive_page,
      wait_receive_nomore,
    } = getState().order

    if (!token || wait_receive_nomore ) return

    const nextPage = wait_receive_page + 1
    asyncOrderList(token, 2,  wait_receive_page).then(({data, status}) => {
      if (status) {
        dispatch(orderLoad({
          wait_receive: wait_receive.concat(data.data),
          wait_receive_page: nextPage,
          wait_receive_nomore:  data.data.length < 5
        }))
      }
    })
  }
}

// 异步的action 加载已完成列表
export function asyncLoadOrderDoneList () {
  return (dispatch, getState) => {
    const { token } = getState().user
    const {
      done, // 已完成
      done_page,
      done_nomore,
    } = getState().order

    if (!token || done_nomore ) return

    const nextPage = done_page + 1
    asyncOrderList(token, 3,  done_page).then(({data, status}) => {
      if (status) {
        dispatch(orderLoad({
          done: done.concat(data.data),
          done_page: nextPage,
          done_nomore:  data.data.length < 5
        }))
      }
    })
  }
}

// 异步的action 加载待评价列表
export function asyncLoadOrderUnpointList () {
  return (dispatch, getState) => {
    const { token } = getState().user
    const {
      wait_point, // 待评价
      wait_point_page,
      wait_point_nomore
    } = getState().order

    if (!token || wait_point_nomore ) return

    const nextPage = wait_point_page + 1
    asyncOrderList(token, 100,  wait_point_page,5).then(({data, status}) => {
      if (status) {
        dispatch(orderLoad({
          wait_point: wait_point.concat(data.data),
          wait_point_page: nextPage,
          wait_point_nomore:  data.data.length < 5
        }))
      }
    })
  }
}

