import {ADDRESS_LIST_LOAD, ADDRESS_SELECT} from '../constants/address'
import {asyncAddressList} from '../api/address'

// 加载地址列表
export const addressListLoad = (addressList) => {
  return {
    type: ADDRESS_LIST_LOAD,
    addressList
  }
}

// 设置选中地址
export const selectAddress = (addressId) => {
  return {
    type: ADDRESS_SELECT,
    select_id: addressId
  }
}


/**
 * 异步的action 加载地址列表
 * @param {boolean} shop_only 是否只加载店铺的
 * @returns {Function}
 */
export function asyncLoadAddressList (shop_only = false) {
  return (dispatch, getState) => {
    const { token } = getState().user
    const { shop_id } = getState().shop
    if (!token) return // TODO

    asyncAddressList(token, shop_only ? shop_id : null).then(({data, status}) => {
      console.log(data)
      if (status) {
        dispatch(addressListLoad(data.data))
      }
    })
  }
}
