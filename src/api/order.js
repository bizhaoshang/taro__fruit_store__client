import Taro from '@tarojs/taro'
import {HTTP_METHOD, SERVER_HOST} from '../constants/server'

/**
 * 创建订单
 * @param {string} token 用户token
 * @param {string} shop_id 店铺id
 * @param {string} type_id  1 普通订单(每日鲜果) 2拼团订单
 * @param {number} total_money 订单实付金额，以分为单位
 * @param {number} total_amount 订单总额
 * @param {number} user_money 扣除用户余额
 * @param {Array.<{product_id: Number, product_name: String, product_num: Number, product_price: Number, total_price: Number}>} products 商品
 * @param {number | null} [user_coupon_id]  用户用券id
 * @param {number | null} [address_id] 地址id 没有则代表自取
 * @returns {Promise<Taro.request.Promised<any> | never>}
 */
export function asyncCreateOrder ({token, shop_id, type_id, total_money, total_amount, user_money, products, user_coupon_id = null,user_voucher_id= null, address_id = null,shipping_fee, form_id,name,phone}) {
  return Taro.request({
    url: `${SERVER_HOST}order/pay`,
    data: {
      token,
      type_id,
      shop_id,
      total_money,
      total_amount,
      user_money,
      products,
      user_coupon_id,
      user_voucher_id,
      address_id,
      shipping_fee,
      form_id,
      name,
      phone
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 创建门店惠买订单
 * @param {string} token 用户token
 * @param {string} shop_id 店铺id
 * @param {string} type_id  1 普通订单(每日鲜果) 2拼团订单
 * @param {number} total_money 订单实付金额，以分为单位
 * @param {number} total_amount 订单总额
 * @param {number} user_money 扣除用户余额
 * @param {Array.<{product_id: Number, product_name: String, product_num: Number, product_price: Number, total_price: Number}>} products 商品
 * @param {number | null} [user_coupon_id]  用户用券id
 * @param {number | null} [address_id] 地址id 没有则代表自取
 * @returns {Promise<Taro.request.Promised<any> | never>}
 */
export function asyncCreateShopOrder({ token, shop_id, type_id, total_money, total_amount, user_money,  user_coupon_id = null, user_voucher_id = null, address_id = null, shipping_fee, form_id, name, phone }) {
  return Taro.request({
    url: `${SERVER_HOST}order/make-shop-order`,
    data: {
      token,
      type_id,
      shop_id,
      total_money,
      total_amount,
      user_money,
     
      user_coupon_id,
      user_voucher_id,
      address_id,
      shipping_fee,
      form_id,
      name,
      phone
    },
    method: HTTP_METHOD
  }).then(({ data }) => {
    return data
  })
}

/**
 * 创建拼团订单
 * @param {string} token 用户token
 * @param {string} shop_id 店铺id
 * @param {number} type_id  1 普通订单(每日鲜果) 2拼团订单
 * @param {number} total_money 订单实付金额，以分为单位
 * @param {number} total_amount 订单总额
 * @param {number} user_money 扣除用户余额
 * @param {product_id: Number, product_name: String, product_num: Number, product_price: Number, total_price: Number} product 商品
 * @param {number | null} [user_coupon_id]  用户用券id
 * @param {number | null} [address_id] 地址id 没有则代表自取
 * @returns {Promise<Taro.request.Promised<any> | never>}
 */
export function asyncCreateGroupOrder ({token, shop_id, type_id = 2, total_money, total_amount, user_coupon_id = null,user_voucher_id= null, user_money, address_id = null,shipping_fee, product = {}, form_id,name,phone}) {
  return Taro.request({
    url: `${SERVER_HOST}order/make-group-order`,
    data: {
      token,
      type_id,
      shop_id,
      total_money,
      total_amount,
      user_money,
      product,
      user_coupon_id,
      user_voucher_id,
      address_id,
      shipping_fee,
      form_id,
      name,
      phone
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 25. 订单数量统计（个人中心里面 “待付款”，“代发货”等）
 * @param {string} token 用户token
 * @returns {Promise<Taro.request.Promised<any> | never>}
 */
export function asyncOrderCount (token) {
  return Taro.request({
    url: `${SERVER_HOST}user/order/count`,
    data: {
      token,
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 取消订单
 * @param {string} token 用户token
 * @param {string} order_code 订单id
 * @returns {Promise<Taro.request.Promised<any> | never>}
 */
export function asyncCancelOrder (token, order_code) {
  return Taro.request({
    url: `${SERVER_HOST}user/order/cancle`,
    data: {
      token,
      order_code
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 查询订单详情
 * @param token
 * @param order_id
 * @returns {Promise<Taro.request.Promised<any> | never>}
 */
export function queryOrderDetail (token, order_id) {
  return Taro.request({
    url: `${SERVER_HOST}user/order/show`,
    data: {
      token,
      order_id
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 *
 * @param {string} token 用户token
 * @param {number | null} status 订单状态 // 1待付款  3待收货   100 待评价
 * @param {number} page 页码
 * @param {number} num 每页数量
 * @returns {Promise<Taro.request.Promised<any> | never>}
 */
export function asyncOrderList (token, status, page, num=5) {

  if(!page){
    page =1
  }
 
  return Taro.request({
    url: `${SERVER_HOST}user/order`,
    data: {
      token,
      status,
      page,
      num
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 评论订单
 * @param token
 * @param order_code
 * @param product_id
 * @param point
 * @returns {Promise<Taro.request.Promised<any> | never>}
 */
export function asyncPointOrder (token, order_code, product_id = [], point =[]) {
  return Taro.request({
    url: `${SERVER_HOST}user/order/point`,
    data: {
      token,
      order_code,
      product_id,
      point
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 33. 订单售后升级  3月3日新增
 * @param token
 * @param order_code
 */
export function asyncOrderService (token, order_code) {
  return Taro.request({
    url: `${SERVER_HOST}user/order/save-cusomerservice`,
    data: {
      token,
      order_code
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 33.   获取小程序码  4月18日新增
 * @param token
 * @param scene_value
 */
export function asyncSmallCode (scene_value) {
  return Taro.request({
    url: `${SERVER_HOST}small-code`,
    data: {
      scene_value
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 33.   获取小程序码-跳转商品详情页面  4月18日新增
 * @param token
 * @param scene_value
 */
export function asyncSmallCodeDetail (scene_value) {
  return Taro.request({
    url: `${SERVER_HOST}small-code`,
    data: {
      scene_value,
      type:"product-info"
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}
