import Taro from "@tarojs/taro";
import {SERVER_HOST, HTTP_METHOD} from "../constants/server"

/**
 * 获取商品的拼团信息
 * @param {string} token 用户token
 * @param {string} product_id 商品id
 * @returns {Promise<Taro.request.Promised<any> | never>}
 */
export function asyncGroupInfo (token, product_id) {

  return Taro.request({
    url: `${SERVER_HOST}product-group`,
    data: {
      token,
      product_id
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 *  发起团购 或者 参加团购
 * @param {string} token 用户token
 * @param {string} product_id 商品id
 * @param {string} product_group_id 拼购活动id
 * @param {string} [parent_id] 团id 参团需要 发起不需要
 * @param {boolean} had_ok 是否保留原团
 * @returns {Promise<Taro.request.Promised<any> | never>}
 */
export function asyncJoinGroup ({token, product_id, product_group_id,  parent_id, had_ok}) {
  return Taro.request({
    url: `${SERVER_HOST}user/groupbuy-create`,
    data: {
      token,
      product_id,
      product_group_id,
      parent_id,
      had_ok
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 取消自己之前创建的团
 * @param token
 * @param group_code
 * @returns {Promise<T | never>}
 */
export function asyncCancelGroup (token, group_code) {
  return Taro.request({
    url: `${SERVER_HOST}user/order/cancle-groupbuy`,
    data: {
      token,
      group_code
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}
