import Taro from "@tarojs/taro";
import {SERVER_HOST, HTTP_METHOD} from "../constants/server"

/**
 * 初次访问获取用户openid
 * @param {string} code 通过微信登录获取的code
 * @param {number} latitude 纬度
 * @param {number} longitude 经度
 * @param {string} avatar 头像地址
 * @param {string} nickname 用户昵称
 * @returns {Promise<{data: *} | never>}
 */
export function asyncGetOpenId (code, latitude, longitude, avatar, nickname) {
  return Taro.request({
    url: `${SERVER_HOST}get-openid`,
    data: {
      code, latitude, longitude, avatar, nickname
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 28.用户详情
 * @param token
 * @returns {Promise<Taro.request.Promised<any> | never>}
 */
export function queryUserInfo (token,shop_id) {
  return Taro.request({
    url: `${SERVER_HOST}user/info`,
    data: {
     token,shop_id
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 30.“我的余额”
 * @param token
 * @param page
 * @returns {Promise<Taro.request.Promised<any> | never>}
 */
export function queryMoneyLog (token, page) {
  return Taro.request({
    url: `${SERVER_HOST}user/money`,
    data: {
      token,
      page
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 获取用户的店铺余额
 * @param token
 * @param shop_id
 * @returns {Promise<Taro.request.Promised<any> | never>}
 */
export function queryShopMoney (token, shop_id) {
  return Taro.request({
    url: `${SERVER_HOST}user/shop-money`,
    data: {
      token,
      shop_id
    },
    method: HTTP_METHOD
  }).then(({data}) => {   
    return data
  })
}

/**
 * 27. 拍十秒红包
 * @param token
 * @param type // 1每日签到拍十秒  2订单完成奖励拍十秒机会
 * @param date // 20181227 必填
 * @param order_code
 * @param second
 * @param shop_id //店铺id
 */
export function getEnvelope ({token, type, date, order_code, shop_id, second}) {
  return Taro.request({
    url: `${SERVER_HOST}user/get-envelope`,
    data: {
      token,
      type: parseInt(type, 10),
      date: parseInt(date, 10),
      order_code,
      shop_id: parseInt(shop_id, 10),
      second
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 32.查询拍十秒的次数
 * @param token
 */
export function getTensecondTimes (token) {
  return Taro.request({
    url: `${SERVER_HOST}user/get-tensecond-times`,
    data: {
      token
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}
