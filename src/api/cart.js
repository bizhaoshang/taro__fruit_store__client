import Taro from "@tarojs/taro";
import {SERVER_HOST, HTTP_METHOD} from "../constants/server"

/**
 * // 获取用户购物车信息
 * @param {string} token 用户token
 * @param {number} page 页数
 * @param {number} num 每页数量
 * @returns {Promise<{data: *} | never>}
 */
export function asyncCart (token, page = 1, num = 10) {
  return Taro.request({
    url: `${SERVER_HOST}user/cart`,
    data: {
      token,
      num,
      page
    },
    method: HTTP_METHOD
  }).then(({data}) => {
   return data
})
}

/**
 * // 添加购物车
 * @param {string} token 用户token
 * @param {string} shop_id 商户id
 * @param {string} product_id 商品id
 * @param {number} product_num 商品数量
 * @returns {Promise<{data: *} | never>}
 */
export function asyncAddCart (token, shop_id, product_id, product_num) {
    return Taro.request({
      url: `${SERVER_HOST}user/cart-create`,
      data: {
        token,
        shop_id,
        product_id,
        product_num
      },
      method: HTTP_METHOD
    }).then(({data}) => {
      return data
    })
}

/**
 * 减少购物车
 * @param {string} token 用户token
 * @param {string} product_id 商品id
 * @returns {Promise<Taro.request.Promised<any> | never>}
 */
export function asyncReduceCart (token, product_id) {
  return Taro.request({
    url: `${SERVER_HOST}user/cart-decrease`,
    data: {
      token,
      product_id
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 删除购物车
 * @param {string} token 用户token
 * @param {string} cart_id 商品在购物车内的id
 * @returns {Promise<Taro.request.Promised<any> | never>}
 */
export function asyncRemoveCart (token, cart_id) {
  return Taro.request({
    url: `${SERVER_HOST}user/cart-delete`,
    data: {
      token,
      cart_id
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}
