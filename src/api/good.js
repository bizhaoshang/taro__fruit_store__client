import Taro from "@tarojs/taro";
import {SERVER_HOST, HTTP_METHOD} from "../constants/server"

/**
 *
 * @param {string} token 用户token
 * @param {number} shop_id 店铺id
 * @param {number} type  1每日鲜果  2社区拼购
 * @param {number} num 数量
 * @returns {Promise<{data: *} | never>}
 */
export function asyncHomeGoodList (token, shop_id, type, num = 6) {
  return Taro.request({
    url: `${SERVER_HOST}rec-product`,
    data: {
      token, shop_id, type, num
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 *
 * @param {string} token 用户token
 * @param {number} shop_id 店铺id
 * @param {number} type_id  1每日鲜果  2社区拼购
 * @param {number} page 页码
 * @param {string} name 搜索词
 * @param {number} num 数量
 * @returns {Promise<{data: *} | never>}
 */
export function asyncGoodList (token, shop_id, type_id, page = 1, name = '', num = 5) {
  return Taro.request({
    url: `${SERVER_HOST}product-list`,
    data: {
      token, shop_id, type_id, page, name, num
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 获取购物车推荐商品
 * @param {string} token 用户token
 * @param {number} shop_id 店铺id
 * @returns {Promise<{data: *} | never>}
 */
export function asyncOfferList(token, shop_id) {
  console.log('api 请求推荐商品')
  return Taro.request({
    url: `${SERVER_HOST}offer-product`,
    data: {
      token, shop_id
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 获取商品详情
 * @param {string} token 用户token
 * @param {number} product_id 商品id
 * @returns {Promise<Taro.request.Promised<any> | never>}
 */
export function asyncGoodDetail (token, product_id) {
  return Taro.request({
    url: `${SERVER_HOST}product-info`,
    data: {
      token, product_id
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 获取 拼团详情
 * @param {string} token 用户token
 * @param {string} user_id 用户id
 * @param {string} group_code 拼团的编码
 * @returns {Promise<{data: *} | never>}
 */
export function asyncUsergroupDetail (token, user_id,group_code) {
  return Taro.request({
    url: `${SERVER_HOST}usergroup-info`,
    data: {
      token, user_id,group_code
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}