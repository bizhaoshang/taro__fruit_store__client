import Taro from "@tarojs/taro";
import {SERVER_HOST, HTTP_METHOD} from "../constants/server"

/**
 * 获取推荐店铺
 * @param {string} token
 * @param {number} latitude 纬度
 * @param {number} longitude 经度
 * @returns {Promise<{data: *} | never>}
 */
export function asyncGetRecommendedShop (token, latitude, longitude) {
  console.log(latitude, longitude)
  return Taro.request({
    url: `${SERVER_HOST}get-shop`,
    data: {
      token, latitude, longitude
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 获取店铺信息
 * @param {string} token
 * @param {number} shop_id 店铺id
 * @returns {Promise<{data: *} | never>}
 */
export function asyncGetShopInfo (token, shop_id) {
  return Taro.request({
    url: `${SERVER_HOST}shop-info`,
    data: {
      token, shop_id
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}
