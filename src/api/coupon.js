import Taro from "@tarojs/taro";
import {HTTP_METHOD, SERVER_HOST} from "../constants/server";

/**
 * 获取用户的优惠券
 * @param {string} token 用户token
 * @param {number} page 页码
 * @param {number} [shop_id] 店铺id
 * @param {number} num 数量
 * @returns {Promise<Taro.request.Promised<any> | never>}
 */
export function asyncUserCoupon (token, page = 1, shop_id = null, num = 10) {
  return Taro.request({
    url: `${SERVER_HOST}user/coupon`,
    data: {
      token, shop_id, page, num
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}
