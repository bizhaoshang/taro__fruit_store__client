import Taro from "@tarojs/taro";
import {SERVER_HOST, HTTP_METHOD} from "../constants/server"

/**
 * 获取系统满减
 * @param {string} token
 * @returns {Promise<{data: *} | never>}
 */
export function asyncWechatCoupon (token) {
  return Taro.request({
    url: `${SERVER_HOST}system-coupon`,
    data: {
      token
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}
