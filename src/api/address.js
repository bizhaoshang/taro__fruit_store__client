import Taro from "@tarojs/taro";
import {HTTP_METHOD, SERVER_HOST} from "../constants/server";

/**
 * 获取用户地址列表
 * @param {string} token 用户token
 * @param {string} [shop_id] 店铺id
 * @returns {Promise<{data: *} | never>}
 */
export function asyncAddressList (token, shop_id) {
  return Taro.request({
    url: `${SERVER_HOST}user/address`,
    data: {
      token,
      shop_id
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 创建地址
 * @param {string} token 用户token
 * @param {string} address 地址
 * @param {string} shop_id 店铺Id
 * @param {string} village 小区名
 * @param {string} gatenum 门牌号
 * @param {string} truename 真实姓名
 * @param {string} phone 电话
 * @param {number} latitude 纬度
 * @param {number} longitude 经度
 * @param {string} gender 性别
 * @param {string} is_default 是否默认地址
 * @param {string} tag 标签
 * @returns {Promise<{data: *} | never>}
 */
export function asyncCreatAddress (token, {address, shop_id, village, gatenum, truename, phone, latitude, longitude, gender, is_default, tag}) {
  return Taro.request({
    url: `${SERVER_HOST}user/address-create`,
    data: {
      token,
      address,
      shop_id,
      village,
      gatenum,
      truename,
      phone,
      latitude,
      longitude,
      gender,
      is_default,
      tag
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 更新地址
 * @param {string} token 用户token
 * @param {number} address_id 原地址id
 * @param {number} shop_id 原店铺id
 * @param {string} address 地址
 * @param {string} village 小区名
 * @param {string} gatenum 门牌号
 * @param {string} truename 真实姓名
 * @param {string} phone 电话
 * @param {number} latitude 纬度
 * @param {number} longitude 经度
 * @param {string} gender 性别
 * @param {string} is_default 是否默认地址
 * @param {string} tag 标签
 * @returns {Promise<{data: *} | never>}
 */
export function asyncUpdateAddress (token, {address_id, shop_id, address, village, gatenum, truename, phone, latitude, longitude, gender, is_default, tag}) {
  return Taro.request({
    url: `${SERVER_HOST}user/address-update`,
    data: {
      token,
      address_id,
      shop_id,
      address,
      village,
      gatenum,
      truename,
      phone,
      latitude,
      longitude,
      gender,
      is_default,
      tag
    },
    method: HTTP_METHOD
  }).then(({data}) => {
    return data
  })
}

/**
 * 收货地址详情
 * @param {string} token 用户token
 * @param {number} address_id 原地址id
 */
export function asyncGetAddress (token, address_id) {
  return Taro.request({
    url: `${SERVER_HOST}user/address-show`,
    data: {
      token,
      address_id
    },
    method: 'POST'
  }).then(({data}) => {
    return data
  })
}
/**
 * 删除地址
 * @param {string} token 用户token
 * @param {number} address_id 原地址id
 */
export function asyncDeleteAddress (token, address_id) {
  return Taro.request({
    url: `${SERVER_HOST}user/address-delete`,
    data: {
      token,
      address_id
    },
    method: 'POST'
  }).then(({data}) => {
    return data
  })
}
