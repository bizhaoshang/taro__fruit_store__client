import Taro, { Component } from '@tarojs/taro'
import {connect} from "@tarojs/redux";
import {View, Image, Text, ScrollView} from '@tarojs/components'
import { asyncGetUserdVoucher, setUserVoucher, selectVoucher } from '../../actions/voucher'
import { asyncShop } from '../../actions/shop'
import './index.styl'

@connect(({ voucher,shop }) => ({
  voucher,shop
}), (dispatch) => ({
  // 加载店铺信息
  onAsyncShop() {
    dispatch(asyncShop())
  },
  // 加载优惠券
  onAsyncUserVoucher (shop_only = true) {
    dispatch(asyncGetUserdVoucher(shop_only))
  },
  // 重置
  onResetUserVoucher () {
    dispatch(setUserVoucher())
  },
  // 选中券
  onSelectVoucher (id) {
    dispatch(selectVoucher(id))
  }
}))
class Voucher extends Component {
  constructor () {
    super(...arguments)
    this.state = {
    }
  }
  config = {
    navigationBarTitleText: '我的代金券',
    onReachBottomDistance: 300
  }

  // 页面到达底部
  onReachBottom () {
    const from = this.$router.params.from
    const shop_only = from === 'settle'
    const {nomore} = this.props.voucher
    if (nomore) return
    this.props.onAsyncUserVoucher(shop_only) // 加载列表
  }

  // 下拉刷新
  onPullDownRefresh () {
    Taro.stopPullDownRefresh()
  }

  // 选中券
  onSelect (id) {
    console.log('选中券')
    this.props.onSelectVoucher(id)
    Taro.navigateBack({ delta: 1 })
  }

  componentDidMount () {
  }

  componentWillReceiveProps (nextProps) {
    console.log(this.props, nextProps)
  }

  componentWillUnmount () { }

  componentDidShow () {
    this.props.onAsyncShop()
    // const from = this.$router.params.from
    // const shop_only = from === 'settle'
    this.props.onResetUserVoucher()
    this.props.onAsyncUserVoucher(true) // 加载列表
  }

  componentDidHide () { }

  render () {
    const { data } = this.props.voucher
    
    const from = this.$router.params.from
    console.log( this.$router.params)
    const amount = parseFloat(this.$router.params.amount) // 用于比较的金额

    const shop_only = from === 'settle'
    console.log(amount, shop_only, from)
    let canUse = [] // 可用券
    let canNotUse = []// 不可用券
    console.log(data,amount,"比较")
    if (shop_only && amount) {
      canUse = data.filter(voucher => (voucher.value * 100) <= amount)
      canNotUse = data.filter(voucher => (voucher.value * 100) > amount)
    }
    console.log(canUse)
    // TODO 数据为空的处理
    const renderList = data.map((item, index) => {
      return (<View className='item' key={index}>
        <View className='left'>
          <Text className='amount'>{item.name}</Text>
        </View>
        <View className='right'>
        <View className='br' />
        <View className='br' />
          <View className='label'>使用规则:</View>
          <View className='value'>有效期至：</View>
          <View className='value'>{item.end_date}</View>
        </View>
      </View>)
    })
    // 不可用券列表
    const canNotUseList = canNotUse.map((item, index) => {
      return (<View className='item' key={index}>
       <View className='left'>
          <Text className='amount'>{item.name}</Text>
        </View>
        <View className='right'>
        <View className='br' />
        <View className='br' />
          <View className='label'>使用规则:</View>
          <View className='value'>有效期至：</View>
          <View className='value'>{item.end_date}</View>
        </View>
      </View>)
    })
    // 可用券列表
    const canUseList = canUse.map((item, index) => {
      return (<View className='item' key={index} onClick={this.onSelect.bind(this, item.id)}>
        <View className='left'>
          <Text className='amount'>{item.name}</Text>
        </View>
        <View className='right'>
        <View className='br' />
        <View className='br' />
          <View className='label'>使用规则:</View>
          <View className='value'>仅限本人使用</View>
          <View className='value'>{item.end_date}</View>
        </View>
      </View>)
    })
    return (
      <View className='coupon'>
        <ScrollView
          className='list'
          enableBackToTop
          scrollY
          scrollWithAnimation
          scrollTop='0'
        >
          {!shop_only && renderList}
          {shop_only && canUseList}
          {shop_only && canNotUse.length > 0 && <View>以下代金券不可用</View>}
          {shop_only && canNotUseList}
          {canUse.length === 0  &&  <View className='no-coupon'>暂无其他可用代金券</View>}
        </ScrollView>
      </View>
    )
  }
}

export default Voucher
