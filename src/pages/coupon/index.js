import Taro, { Component } from '@tarojs/taro'
import {connect} from "@tarojs/redux";
import {View, Image, Text, ScrollView} from '@tarojs/components'
import { asyncGetUserCoupoon, resetUserCoupon, selectCoupon } from '../../actions/coupon'
import './index.styl'

@connect(({ coupon }) => ({
  coupon
}), (dispatch) => ({
  // 加载优惠券
  onAsyncUserCoupon (shop_only = false) {
    dispatch(asyncGetUserCoupoon(shop_only))
  },
  // 重置
  onResetUserCoupon () {
    dispatch(resetUserCoupon())
  },
  // 选中券
  onSelectCoupon (id) {
    dispatch(selectCoupon(id))
  }
}))
class Coupon extends Component {
  constructor () {
    super(...arguments)
    this.state = {
    }
  }
  config = {
    navigationBarTitleText: '我的卡券',
    onReachBottomDistance: 300
  }

  // 页面到达底部
  onReachBottom () {
    const from = this.$router.params.from
    const shop_only = from === 'settle'
    const {nomore} = this.props.coupon
    if (nomore) return
    this.props.onAsyncUserCoupon(shop_only) // 加载列表
  }

  // 下拉刷新
  onPullDownRefresh () {
    Taro.stopPullDownRefresh()
  }

  // 选中券
  onSelect (id) {
    console.log('选中券')
    this.props.onSelectCoupon(id)
    Taro.navigateBack({ delta: 1 })
  }

  componentDidMount () {
  }

  componentWillReceiveProps (nextProps) {
    console.log(this.props, nextProps)
  }

  componentWillUnmount () { }

  componentDidShow () {
    const from = this.$router.params.from
    const shop_only = from === 'settle'
    this.props.onResetUserCoupon()
    this.props.onAsyncUserCoupon(shop_only) // 加载列表
  }

  componentDidHide () { }

  render () {
    const { list } = this.props.coupon
    const from = this.$router.params.from
    const amount = parseFloat(this.$router.params.amount) // 用于比较的金额
    const shop_only = from === 'settle'
    console.log(amount, shop_only, from)
    let canUse = [] // 可用券
    let canNotUse = []// 不可用券
    if (shop_only && amount) {
      canUse = list.filter(coupon => (coupon.coupon.min_amount * 100) <= amount)
      canNotUse = list.filter(coupon => (coupon.coupon.min_amount * 100) > amount)
    }
    console.log(canUse)
    // TODO 数据为空的处理
    const renderList = list.map((item, index) => {
      return (<View className='item' key={index}>
        <View className='left'>
          <Text className='symbol'>￥</Text>
          <Text className='amount'>{item.coupon.value}</Text>
        </View>
        <View className='right'>
          <View className='label'>过期日期:</View>
          <View className='value'>{item.end_date}</View>
          <View className='br' />
          <View className='label'>使用规则:</View>
          <View className='value'>仅限店铺 {item.shop.name} 内使用</View>
          <View className='value'>满{item.coupon.min_amount}减{item.coupon.value}</View>
        </View>
      </View>)
    })
    // 不可用券列表
    const canNotUseList = canNotUse.map((item, index) => {
      return (<View className='item' key={index}>
        <View className='left'>
          <Text className='symbol'>￥</Text>
          <Text className='amount'>{item.coupon.value}</Text>
        </View>
        <View className='right'>
          <View className='label'>过期日期:</View>
          <View className='value'>{item.end_date}</View>
          <View className='br' />
          <View className='label'>使用规则:</View>
          <View className='value'>仅限店铺 {item.shop.name} 内使用</View>
          <View className='value'>满{item.coupon.min_amount}减{item.coupon.value}</View>
        </View>
      </View>)
    })
    // 可用券列表
    const canUseList = canUse.map((item, index) => {
      return (<View className='item' key={index} onClick={this.onSelect.bind(this, item.id)}>
        <View className='left'>
          <Text className='symbol'>￥</Text>
          <Text className='amount'>{item.coupon.value}</Text>
        </View>
        <View className='right'>
          <View className='label'>过期日期:</View>
          <View className='value'>{item.end_date}</View>
          <View className='br' />
          <View className='label'>使用规则:</View>
          <View className='value'>仅限店铺 {item.shop.name} 内使用</View>
          <View className='value'>满{item.coupon.min_amount}减{item.coupon.value}</View>
        </View>
      </View>)
    })
    return (
      <View className='coupon'>
        <ScrollView
          className='list'
          enableBackToTop
          scrollY
          scrollWithAnimation
          scrollTop='0'
        >
          {!shop_only && renderList}
          {shop_only && canUseList}
          {shop_only && canNotUse.length > 0 && <View>以下券不可用</View>}
          {shop_only && canNotUseList}
          {canUse.length === 0  &&  <View className='no-coupon'>暂无可用优惠券</View>}
        </ScrollView>
      </View>
    )
  }
}

export default Coupon
