import Taro, { Component } from '@tarojs/taro'
import {connect} from "@tarojs/redux";
import { AtRate, AtButton } from 'taro-ui'
import {View, Image, Text, ScrollView} from '@tarojs/components'
import './point.styl'
import {queryOrderDetail, asyncPointOrder} from '../../api/order'
import {IMAGE_HOST} from "../../constants/server";

@connect(({ user, order }) => ({
  user, order
}))
class Order extends Component {
  constructor () {
    super(...arguments)
    this.state = {
      products: [], // 商品
      code: '' // 订单code
    }
  }
  config = {
    navigationBarTitleText: '发表评论',
  }


  // 加载商品
  loadOrder () {
    const {token} = this.props.user
    const {orderId} = this.$router.params // 订单id
    queryOrderDetail(token, orderId).then(({status, data}) => {
      if (!status) {
        Taro.showToast({
          title: '获取失败',
          icon: 'none'
        })
        return
      }
      const {products, code} = data
      this.setState({
        code: code,
        products: products.map(product => {
          return Object.assign({}, product, {point: 3})
        })
      })
    })
  }

  handleChange (index, value) {
    const clone = JSON.parse(JSON.stringify(this.state.products))
    clone[index].point = value
    this.setState({
      products: clone
    })
  }

  handleSubmit () {
    const {token} = this.props.user
    const {code, products} = this.state
    const product_id = products.map(product => {
      return product.product_id
    })
    const point = products.map(product => {
      return product.point
    })
    asyncPointOrder(token, code, product_id, point).then(({status, data}) => {
      console.log(status, data)
      if (!status) {
        Taro.showToast({
          title: '提交失败',
          icon: 'none'
        })
        return
      }
      Taro.navigateBack({ delta: 1 })
    })
  }

  componentDidMount () {
    this.loadOrder()
  }

  componentWillReceiveProps (nextProps) {
    console.log(this.props, nextProps)
  }

  componentWillUnmount () { }

  componentDidShow () {
    this.loadOrder()
  }

  componentDidHide () { }

  render () {
    const {products} = this.state
    const renderProducts = products.map((product, index) => {
      return (<View className='order-point-item' key={index}>
        <Image
          className='order-point-item__thumb'
          style='background: #fff;'
          src={product.product_thumb ? (product.product_thumb) : 'https://img.icons8.com/dusk/64/000000/raspberry.png'}
        />
        <View className='order-point-right'>
          <View className='order-point-right__name'>{product.product_name}</View>
          <View className='order-point-right__point'>
            <AtRate
              value={product.point}
              onChange={this.handleChange.bind(this, index)}
            />
          </View>
        </View>
      </View>)
    })
    return (
      <View className='order-point'>
        <ScrollView className='order-point-list'>
          {renderProducts}
        </ScrollView>
        {/*底部*/}
        <View className='footer' onClick={this.handleSubmit.bind(this)}>
          <Text>提交</Text>
        </View>
      </View>
    )
  }
}

export default Order
