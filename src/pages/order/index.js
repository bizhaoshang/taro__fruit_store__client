import Taro, { Component } from '@tarojs/taro'
import {connect} from "@tarojs/redux";
import {AtTabs, AtTabsPane, AtButton, AtActionSheet, AtActionSheetItem} from 'taro-ui'
import {View, Image, Text, ScrollView, Canvas} from '@tarojs/components'
import drawQrcode from 'weapp-qrcode';
import Poster from '../../components/poster/poster/poster'
import Poster2 from '../../components/poster/poster/posterfull'
import withShare from '../../components/withShare'
import './index.styl'
import OrderListItem from '../../components/orderListItem'
import {
  asyncLoadOrderAllList,
  asyncLoadOrderUnpayList,
  asyncLoadOrderUnreceiveList,
  asyncLoadOrderUnpointList,
  asyncLoadOrderDoneList,
  orderReset,
  orderLoad,
  orderRefresh
} from '../../actions/order'
import {asyncCancelOrder, queryOrderDetail,asyncSmallCode,asyncSmallCodeDetail} from '../../api/order'
import {jumpUrl} from "../../utils/router";
import defaultShareImg from "../../assets/images/share.jpg";
import shareCodeImg from "../../assets/images/code.jpeg";
import pinIcon from '../../assets/images/pinicon.png'
import locationImg from '../../assets/images/pin.png'
import shopImg from '../../assets/images/shop.png'
import shareCardBg from '../../assets/images/sharecardbg.png'

@withShare({})
@connect(({ user, order,good }) => ({
  user, order,good
}), (dispatch) => ({

  // 加载店铺信息
  onAsyncShop() {
    dispatch(asyncShop())
  },
  // 加载 '全部' 列表
  onLoadOrderAllList (pageNum) {
    dispatch(asyncLoadOrderAllList(pageNum))
  },
  // 加载 '待支付' 列表
  onLoadOrderUnpayList () {
    dispatch(asyncLoadOrderUnpayList())
  },
  // 加载 '待收货' 列表
  onLoadOrderUnreceiveList () {
    dispatch(asyncLoadOrderUnreceiveList())
  },
  // 加载 '待评价' 列表
  onLoadOrderUnpointList () {
    dispatch(asyncLoadOrderUnpointList())
  },
  // 加载 '已完成' 列表
  onLoadOrderDoneList () {
    dispatch(asyncLoadOrderDoneList())
  },
  // 重置
  onOrderReset (type) {
    dispatch(orderReset(type))
  },
  // 直接修改
  onOrderSet (data) {
    dispatch(orderLoad(data))
  },
  // 设置需要刷新订单列表
  onOrderRefresh (bool) {
    dispatch(orderRefresh(bool))
  }
}))
class Order extends Component {
  constructor () {
    super(...arguments)
    this.state = {
      pageNum:1,
      goodId:null,
      numValue:null,
      scene_value:"",
      smallCodeImg: '',
      currentTab: 0,
      show_qrcode: false,
      list_name: 'all',
      pageinit: false,
      posterImg: '', // 分享图
      shareImg: defaultShareImg,
      shareActionSheetOpen: false, // 分享action
      shareGroup: null, // 被分享的团购
      posterConfig : {
        width: 1000,
        height: 800,
        backgroundColor: '#fff',
        debug: false,
        images: [
          {
            url: defaultShareImg,
            width: 800,
            height: 800,
            y: 0,
            x: 100,
          }
        ]
      },
      posterConfig2: { // 海报图配置
        width: 750,
        height: 1334,
        backgroundColor: '#fff',
        debug: false,
        texts: [
          // 名称
          {
            x: 750 / 2,
            y: 750 * 1.1,
            text: `社区生活家`,
            fontSize: Math.floor(750 * 0.06),
            color: '#555',
            opacity: 1,
            zIndex: 9,
            textAlign: 'center',
            width: 650
          },
          // 价格
          {
            x: 750 / 2,
            y: 750 * 1.17,
            text: `新鲜低价`,
            fontSize:  Math.floor(750 * 0.05),
            color: '#FD7474',
            opacity: 1,
            zIndex: 9,
            textAlign: 'center',
            width: 650
          },
          // 描述
          {
            x: 750 / 2,
            y: 750 * 1.24,
            text: `长按识别小程序二维码`,
            fontSize: Math.floor(750 * 0.04),
            color: '#B2B2B2',
            opacity: 1,
            zIndex: 9,
            textAlign: 'center',
            width: 650
          },
        ],
        images: [
          {
            url:  defaultShareImg,
            width: 750,
            height: 750,
            y: 0,
            x: 0,
          },
          {
            url: shareCodeImg,
            width: 750 * .4,
            height: 750 * .4,
            y: 750 * 1.3,
            x: Math.floor(750 * .3),
          }
        ]
      },
    }
  }
  config = {
    navigationBarTitleText: '我的订单',
    onReachBottomDistance: 1,
    usingComponents: {
      'poster': '../../components/poster/poster/index',
    }
  }

  // 设置分享标题
  $setShareTitle = () => {
    if (!this.state.shareGroup) {
      return ''
    }
    const { spec, weight, name } = this.state.shareGroup.product
    return `${name}`
  }

  // 设置分享图
  $setShareImageUrl = () => {
    const {  shareImg } = this.state
    console.log('$setShareImageUrl', shareImg)
    return shareImg
  }
  // 设置分享连接
  $setShareUrl= () => {
    const {  numValue,scene_value,goodId } = this.state
     const { user } = this.props;
    let setShareUrlA = ""
    if(numValue>0){
      setShareUrlA =`pages/groupBuy/show?&scene=${scene_value}`;
    }else{
      setShareUrlA =`pages/good/index?&shareFromUser=${user.openid}&id=${goodId}&from=list`;
    }
    return setShareUrlA
  }

  // 页面到达底部
  onReachBottom () {
    Taro.stopPullDownRefresh()
    const {currentTab} = this.state
  
   this.state.pageNum++
     
    
    console.info('当前是第几页：' + this.state.pageNum);
    this.loadTab(currentTab, true, false,this.state.pageNum)

  }

  // 下拉刷新
  onPullDownRefresh () {
    Taro.stopPullDownRefresh()
    const {currentTab} = this.state
    if(this.state.pageNum>1) {
      this.state.pageNum--
    }
    
    this.loadTab(currentTab, true, true, this.state.pageNum)

  }

  // tab 切换
  handleTabClick (value) {
    this.loadTab(value, false, true)
  }

  // 加载
  loadTab (value, force = false, reset = true ,pageNum) {
    const {currentTab} = this.state
    if (currentTab === value && !force) return

    let list_name = 'all'
  
    switch (value) {
      case  0: // 全部
        list_name = 'all'
        reset && this.props.onOrderReset('all')
        this.props.onLoadOrderAllList(pageNum)
        break
      // case 1: // 待付款
      //   list_name = 'wait_pay'
      //   reset && this.props.onOrderReset('wait_pay')
      //   this.props.onLoadOrderUnpayList()
      //   break
      // case 2: // 待收货
      //   list_name = 'wait_receive'
      //   reset && this.props.onOrderReset('wait_receive')
      //   this.props.onLoadOrderUnreceiveList()
      //   break
      // case 3: // 已完成
      //   list_name = 'done'
      //   reset && this.props.onOrderReset('done')
      //   this.props.onLoadOrderDoneList()
      //   break
      case 1: // 待评价
        list_name = 'wait_point'
        reset && this.props.onOrderReset('wait_point')
        this.props.onLoadOrderUnpointList()
        break
    }
    this.setState({
      currentTab: value,
      list_name
    })
  }

  // 拍十秒
  onMinute (order_id, shop_id, order_code) {
    Taro.navigateTo({
      url: `/pages/game/minute?type=2&shop_id=${shop_id}&order_code=${order_code}`
    })
  }

  // 取消订单
  onOrderCancel (order_code, orderId) {
    // 注意：无论用户点击确定还是取消，Promise 都会 resolve。
    Taro.showModal({
      title: '提示',
      content: '您确定要取消此订单吗?',
      cancelText: '算了',
      confirmText: '确定'
    })
      .then(
        res => {
          if (res.confirm) {
            const { token } = this.props.user
            asyncCancelOrder(token, order_code)
              .then(({ status, data }) => {
                if (!status) {
                  throw '操作失败'
                }
                return queryOrderDetail(token, orderId)
              }
              )
              .then(({ status, data }) => {
                if (status) {
                  let list_name = this.state.list_name
                  const list = this.props.order[list_name]
                  const new_list = JSON.parse(JSON.stringify(list))
                  const index = new_list.findIndex(order => order.id === orderId)
                  new_list.splice(index, 1, data)
                  this.props.onOrderSet({
                    [list_name]: new_list
                  })
                }
              })
              .catch(() => {
                Taro.showToast({
                  title: '操作失败',
                  icon: 'none'
                })
              })
          }
        })
     
    
  }

  // 展示二维码
  onFinishOrder (orderId) {
    this.setState({
      show_qrcode: true
    })
    setTimeout(async () => {
      // 设置屏幕比例
      const res = await Taro.getSystemInfo();
      const scale = res.screenWidth / 375;
      console.log(orderId)
      drawQrcode({
        width: 200 * scale,
        height: 200 * scale,
        canvasId: 'myQrcode',
        _this: this.$scope,
        text: String(orderId)
      });
    }, 200)
  }

  // 关闭二维码
  onCloseQR (e) {
    console.log('onCloseQR')
    e.stopPropagation()
    e.preventDefault()
    this.setState({
      show_qrcode: false
    })
  }

  // 阻止穿透点击
  onStopClick = (e) => {
    e.stopPropagation()
    e.preventDefault()
  }

  // 跳转
  jumpTo (url) {
    jumpUrl(url, {method: 'navigateTo'})
  }

  // 评价商品
  onPointOrder (orderId) {
    this.jumpTo(`/pages/order/point?orderId=${orderId}`)
  }

  // 团购分享
  onGroupShare (shareGroup,orderId,numValue,shopId) {
    // 分享按钮是点击后马上触发的，先生成分享图，然后展示actionsheet，用户点击了actionsheet里的分享直接使用分享图。
    const {product, group_price } = shareGroup
    const { month_sale, origin_price, thumb, spec_name,weight,price,sale_price} = product
    const {token,origin} = this.props.user
    //  console.log(group.need_num - group.user_num)
    queryOrderDetail(token, orderId)
      .then(({status, data}) => {
       let scene_value =data.group_code+"-"+data.user_id
       this.setState({
        goodId:product.id,
        numValue : numValue,
        scene_value : scene_value
      })
       if(numValue>0){
        asyncSmallCode( scene_value)
        .then(({status, data}) => {
          this.setState({
            smallCodeImg : origin +"/"+ data
          })
        })
       }else{
        asyncSmallCodeDetail(shopId)
        .then(({statusA, data}) => {
          this.setState({
            smallCodeImg : origin +"/"+ data
          })
        })
       }
      })
      .catch(() => {
        Taro.showToast({
          title: '操作失败',
          icon: 'none'
        })
      })
    this.setState({
      shareGroup,
      posterConfig: {
        width: 1000,
        height: 800,
        backgroundColor: '#fff',
        debug: false,
        blocks: [
          {
            x: 0,
            y: 600,
            width: 1000, // 如果内部有文字，由文字宽度和内边距决定
            height: 200,
            paddingLeft: 0,
            paddingRight: 0,
            borderWidth: 0,
            backgroundColor: '#FFC93E',
            borderRadius: 40,
            zIndex: 1,
          },
          {
            x: 0,
            y: 760,
            width: 40,
            height: 40,
            paddingLeft: 0,
            paddingRight: 0,
            borderWidth: 0,
            backgroundColor: '#FFC93E',
            borderRadius: 0
          },
          {
            x: 960,
            y: 760,
            width: 40,
            height: 40,
            paddingLeft: 0,
            paddingRight: 0,
            borderWidth: 0,
            backgroundColor: '#FFC93E',
            borderRadius: 0
          }
        ],
        texts: [
          {
            x: 50,
            y: 660,
            text: [
              {
                text: `¥ ${parseFloat(group_price / 100).toFixed(2)}`,
                fontSize: 80,
                color: 'white',
                opacity: 1,
                marginLeft: 0,
                marginRight: 40,
                lineHeight: 140,
                lineNum: 1,
              },
              {
                text: `¥${parseFloat(sale_price / 100).toFixed(2)}`,
                fontSize: 56,
                color: '#B2B2B2',
                marginLeft: 20,
                textDecoration: 'line-through'
              }
            ],
            zIndex: 9,
            baseLine: 'middle',
          },
          {
            x: 50,
            y: 740,
            text: ` ${weight}g/${spec_name}`,
            fontSize: 50,
            color: 'white',
            opacity: 1,
            baseLine: 'middle',
            zIndex: 9,
          },
        ],
        images: [
          {
            url: thumb || defaultShareImg,
            width: 800,
            height: 800,
            y: 0,
            x: 100,
          },
          {// 拼图标
            url: pinIcon,
            width: 120,
            height: 120,
            y: 20,
            x:20,
            zIndex: 9,
          }
        ]
      }
    })

    // 这里要求Poster绘图，会在成功的回调里打开actionsheet。用户点击了actionsheet里的分享直接使用分享图。
    setTimeout(() => {
      Poster.create(true)    // 入参：true为抹掉重新生成
    }, 200)
    Taro.showLoading({})

  }

  // 图片生成成功
  onPosterSuccess (preview, e) {
    const { detail } = e;
    console.log(this.state)
    console.log(detail)
    Taro.hideLoading()
    // 预览
    if (preview === 'poster') {
      this.setState({
        posterImg: detail,
        shareActionSheetOpen: false
      })
      Taro.previewImage({
        current: detail,
        urls: [detail]
      })
    }
    // 分享
    if (preview === 'share') {
      this.setState({
        shareImg: detail,
        shareActionSheetOpen: true
      })
    }
  }

  // 图片生成失败
  onPosterFail () {
    console.log('失败')
    Taro.hideLoading()
  }

  handleActionSheetOpen () {
    this.setState({
      shareActionSheetOpen: true
    })
  }

  // 分享actionsheet 关闭
  handleActionSheetClose () {
    this.setState({
      shareActionSheetOpen: false
    })
  }

  // 分享actionsheet 取消
  handleActionSheetCancel () {
    this.setState({
      shareActionSheetOpen: false,
      shareImg: defaultShareImg
    })
  }

  // 分享actionsheet 点击
  handleActionSheetClick (type) {
    console.log("分享actionsheet 点击")
    this.setState({
      shareActionSheetOpen: false
    })
    const {shareGroup} = this.state
    if (type === 'poster') {
      const {product, need_num, group_price, shop} = shareGroup
      const { spec, weight, tags, month_sale, name, origin_price, thumb ,con_price,sale_price} = product
      const {avatar, nickname} = this.props.user
      Taro.getSystemInfo({}).then(({screenWidth, screenHeight, pixelRatio}) => {
        const width = screenWidth * pixelRatio
        const height = Math.max(screenHeight * pixelRatio, Math.floor(width * 2.1))
        const codeWidth = Math.floor(width * .4)
        const topheight = 150
        const infoblockH = width * 0.6 + 390 + topheight
        const textxLeft = 40
        let tags_width = 0
        this.setState({
          posterConfig2: {
            width,
            height:height - 50,
            backgroundColor: '#000',
            debug: false,
            blocks: [{
              // 商品规格
              x: width - 230,
              y: width * 0.6 + 220 + topheight,
              width: 200,
              height: Math.floor(width * 0.04) * 1.5,
              text: {
                text: `${weight}g/${spec.name}`,
                fontSize: Math.floor(width * 0.04),
                lineHeight: Math.floor(width * 0.04) * 1.5,
                color: '#0f59a4',
                opacity: 1,
                zIndex: 9,
                textAlign: 'right',
              }
            },
              // tags
              ...tags.map((tag, index) => {
                const  fontSize = Math.floor(width * 0.03)
                const tag_width = fontSize * tag.name.length * 1.1 + 40
                const tag_height = fontSize * 1.5
                tags_width = tags_width + tag_width + 30
                return {
                  x:  tags_width - tag_width,
                  y: width * 0.6 + 280 + topheight,
                  width: tag_width,
                  height: tag_height,
                  backgroundColor: '#FFD701',
                  borderRadius: 10,
                  zIndex: 9,
                  text: {
                    text: `${tag.name}`,
                    fontSize: fontSize,
                    lineHeight: fontSize * 1.5,
                    color: '#fff',
                    opacity: 1,
                    textAlign: 'center',
                    baseLine: 'middle'
                  }
                }
              }),
              // 分界线
              {
                width: width * 0.96,
                height: 1,
                x: width * 0.02,
                y: width * 0.6 + 360 + topheight,
                backgroundColor: '#C7C7C7',
              },
            ],
            texts: [
              {
                // 微信昵称
                x: 130,
                y: 70 + topheight,
                text: nickname,
                fontSize: Math.floor(width * 0.05),
                lineHeight: 80,
                baseLine: 'middle',
                color: '#555',
                opacity: 1,
                zIndex: 9,
                textAlign: 'left',
                width: width - 160
              },
              // 商品名称
              {
                x: textxLeft,
                y: width * 0.6 + 230 + topheight,
                text: `${name}`,
                fontSize: Math.floor(width * 0.05),
                color: '#555',
                opacity: 1,
                zIndex: 9,
                textAlign: 'left',
                width: width - 260
              },
              // 人数
              {
                x: textxLeft +20,
                y: infoblockH + 38,
                baseLine: 'middle',
                text: [
                  {
                    text: `${need_num}人`,
                    fontSize: Math.floor(width * 0.06),
                    color: '#ec1731',
                    marginLeft: Math.floor(width * 0.08) +20,
                  }
                ]
              },
               // 价格
               {
                x: width - 390,
                y: infoblockH + 38,
                baseLine: 'middle',
                textAlign: 'left',
                text: [
                    {
                        text: `¥${parseFloat(group_price / 100).toFixed(2)}`,
                        fontSize: Math.floor(width * 0.06),
                        color: '#ec1731',
                        marginLeft: 30,
                    },
                    {
                        text: `¥${parseFloat(sale_price / 100).toFixed(2)}`,
                        fontSize: Math.floor(width * 0.04),
                        color: '#B2B2B2',
                        marginLeft: 30,
                        textDecoration: 'line-through'
                    }
                ]
               },
              // 描述
              {
                x: width / 2,
                y: width * 2 + topheight,
                text: `长按识别小程序二维码`,
                fontSize: Math.floor(width * 0.04),
                color: '#B2B2B2',
                opacity: 1,
                zIndex: 9,
                textAlign: 'center',
                width: width - 100
              },
              // 店铺名称
              {
                x: 50 + Math.floor(width * 0.06),
                y: width * 1.29 + topheight,
                text: `${shop.name}`,
                fontSize: Math.floor(width * 0.04),
                color: '#515151',
                opacity: 1,
                zIndex: 9,
                textAlign: 'left',
                baseLine: 'baseline',
                width: width - 80 - Math.floor(width * 0.05)
              },
              // 店铺地址
              {
                x: 50 + Math.floor(width * 0.06),
                y: width * 1.39 + topheight,
                lineHeight: Math.floor(width * 0.04)+10,
                text: `${shop.address}`,
                fontSize: Math.floor(width * 0.04),
                color: '#515151',
                opacity: 1,
                zIndex: 9,
                textAlign: 'left',
                baseLine: 'baseline',
                width: width - 80 - Math.floor(width * 0.05)
              },
            ],
            images: [
              { // 背景图片
                  url: shareCardBg,
                  width: width,
                  height: height - 50,
                  position: 'absolute',
                  y: topheight,
                  x: 0,
                  zIndex: 0,

              },
              { // 商品大图
                url: thumb || defaultShareImg,
                width: width * 0.8,
                height: width * 0.6,
                y: 150 + topheight,
                x: width * 0.1,
              },
              { // 用户头像
                url: avatar ,
                width: 80,
                height: 80,
                y: 30 + topheight,
                x: textxLeft,
                borderRadius: 80,
                // borderWidth: 1
              },
              { // shop图标
                url: shopImg,
                width: Math.floor(width * 0.05),
                height: Math.floor(width * 0.05),
                y: width * 1.24 + topheight,
                x: textxLeft
              },
              { // 位置 图标
                url: locationImg,
                width: Math.floor(width * 0.05),
                height: Math.floor(width * 0.05),
                y: width * 1.34 + topheight,
                x: textxLeft
              },
              { // 拼图标
                  url: pinIcon,
                  width: Math.floor(width * 0.08),
                  height: Math.floor(width * 0.08),
                  y: infoblockH,
                  x: textxLeft
              },
              { // 二维码图
                url: this.state.smallCodeImg,
                width: codeWidth,
                height: codeWidth,
                y: width * 1.5 + topheight,
                x: Math.floor(width * .3),
              }
            ]
          }
        })
        setTimeout(() => {
          Poster2.create(true)    // 入参：true为抹掉重新生成
        }, 200)
      })
      Taro.showLoading({})
    }
  }


  componentDidMount () {
    let currentTab = this.$router.params.tab || 0
    currentTab = parseInt(currentTab, 10)
    this.setState({
      currentTab,
      pageinit: true
    })
    this.loadTab(currentTab, true, true)
    this.props.onOrderRefresh(false)
  }

  componentWillReceiveProps (nextProps) {
    console.log(this.props, nextProps)
  }

  componentWillUnmount () { }

  componentDidShow () {
    const { need_refresh } = this.props.order
    console.log('need_refresh', need_refresh)
    if (need_refresh) {
      const {currentTab, pageinit} = this.state
      if (pageinit) {
        this.props.onOrderRefresh(false)
        this.loadTab(currentTab, true, true)
      }
    }
  }
  componentDidHide () { }

  render () {
    console.log('订单页被渲染')
    // /*TODO 上拉加载 下拉刷新*/
    const {currentTab, show_qrcode, posterConfig, posterConfig2, shareActionSheetOpen} = this.state
    // const tabList = [{ title: '全部' }, { title: '待确认' }, { title: '待收货' }, { title: '已完成' }, { title: '待评价' }]
    const tabList = [{ title: '全部' }, { title: '待评价' }]
    const {
      all, // 全部列表
      wait_pay, // 待支付
      wait_point, // 待评价
      wait_receive, // 待收货
      done, // 已完成
    } = this.props.order
    console.log('订单页被渲染',all)
    // 全部列表
    const renderAllList = all.map((order, index) => {
      return (<OrderListItem
        orderId={order.id}
        shopId={order.shop_id}
        key={index}
        code={order.code}
        user_status={order.user_status}
        order_status={order.status}
        tenseconds={order.tenseconds}
        products={order.products}
        address={order.address}
        created_at={order.created_at}
        completed_at={order.completed_at} // 完成时间
        total_amount={order.total_amount} // 订单总额
        total_money={order.total_money} // 订单实付金额
        shipping_fee={order.shipping_fee} //配送费
        onOrderCancel={this.onOrderCancel.bind(this)}
        onFinishOrder={this.onFinishOrder.bind(this)}
        onPointOrder={this.onPointOrder.bind(this)}
        onMinute={this.onMinute.bind(this)}
        group={order.group}
        onGroupShare={this.onGroupShare.bind(this, order.group)}
      />)
    })
    // 待支付 列表
    const renderWaitPayList = wait_pay.map((order, index) => {
      return (<OrderListItem
        orderId={order.id}
        shopId={order.shop_id}
        key={index}
        code={order.code}
        user_status={order.user_status}
        order_status={order.status}
        tenseconds={order.tenseconds}
        products={order.products}
        address={order.address}
        created_at={order.created_at}
        completed_at={order.completed_at} // 完成时间
        total_amount={order.total_amount} // 订单总额
        total_money={order.total_money} // 订单实付金额
        shipping_fee={order.shipping_fee} //配送费
        onOrderCancel={this.onOrderCancel.bind(this)}
        onFinishOrder={this.onFinishOrder.bind(this)}
        onPointOrder={this.onPointOrder.bind(this)}
        onMinute={this.onMinute.bind(this)}
        group={order.group}
      />)
    })
    // 待评价 列表
    const renderWaitPointList = wait_point.map((order, index) => {
      return (<OrderListItem
        orderId={order.id}
        shopId={order.shop_id}
        key={index}
        code={order.code}
        user_status={order.user_status}
        order_status={order.status}
        tenseconds={order.tenseconds}
        products={order.products}
        address={order.address}
        created_at={order.created_at}
        completed_at={order.completed_at} // 完成时间
        total_amount={order.total_amount} // 订单总额
        total_money={order.total_money} // 订单实付金额
        shipping_fee={order.shipping_fee} //配送费
        onOrderCancel={this.onOrderCancel.bind(this)}
        onFinishOrder={this.onFinishOrder.bind(this)}
        onPointOrder={this.onPointOrder.bind(this)}
        onMinute={this.onMinute.bind(this)}
        // group={order.group}
      />)
    })
    // 待收货 列表
    const renderWaitReceiveList = wait_receive.map((order, index) => {
      return (<OrderListItem
        orderId={order.id}
        shopId={order.shop_id}
        key={index}
        code={order.code}
        user_status={order.user_status}
        order_status={order.status}
        tenseconds={order.tenseconds}
        products={order.products}
        address={order.address}
        created_at={order.created_at}
        completed_at={order.completed_at} // 完成时间
        total_amount={order.total_amount} // 订单总额
        total_money={order.total_money} // 订单实付金额
        shipping_fee={order.shipping_fee} //配送费
        onOrderCancel={this.onOrderCancel.bind(this)}
        onFinishOrder={this.onFinishOrder.bind(this)}
        onPointOrder={this.onPointOrder.bind(this)}
        onMinute={this.onMinute.bind(this)}
        group={order.group}
      />)
    })
    // 已完成 列表
    const renderDoneList = done.map((order, index) => {
      return (<OrderListItem
        orderId={order.id}
        shopId={order.shop_id}
        key={index}
        code={order.code}
        user_status={order.user_status}
        order_status={order.status}
        tenseconds={order.tenseconds}
        products={order.products}
        address={order.address}
        created_at={order.created_at}
        completed_at={order.completed_at} // 完成时间
        total_amount={order.total_amount} // 订单总额
        total_money={order.total_money} // 订单实付金额
        shipping_fee={order.shipping_fee} //配送费
        onOrderCancel={this.onOrderCancel.bind(this)}
        onFinishOrder={this.onFinishOrder.bind(this)}
        onPointOrder={this.onPointOrder.bind(this)}
        onMinute={this.onMinute.bind(this)}
        group={order.group}
      />)
    })

    return (
      <View className='order'>
        <AtTabs
          animated={false}
          swipeable={false}
          current={currentTab}
          tabList={tabList}
          onClick={this.handleTabClick.bind(this)}
        >
          {/*全部*/}
          <AtTabsPane current={currentTab} index={0} >
            <ScrollView className='order-list'>
              {renderAllList}
            </ScrollView>
          </AtTabsPane>
          {/*待支付*/}
          {/*<AtTabsPane current={currentTab} index={1}>*/}
            {/*<ScrollView className='order-list'>*/}
              {/*{renderWaitPayList}*/}
            {/*</ScrollView>*/}
          {/*</AtTabsPane>*/}
          {/*待收货*/}
          {/*<AtTabsPane current={currentTab} index={2}>*/}
            {/*<ScrollView className='order-list'>*/}
              {/*{renderWaitReceiveList}*/}
            {/*</ScrollView>*/}
          {/*</AtTabsPane>*/}
          {/*已完成*/}
          {/*<AtTabsPane current={currentTab} index={3}>*/}
            {/*<ScrollView className='order-list'>*/}
              {/*{renderDoneList}*/}
            {/*</ScrollView>*/}
          {/*</AtTabsPane>*/}
          {/*待评价*/}
          <AtTabsPane current={currentTab} index={1}>
            <ScrollView className='order-list'>
              {renderWaitPointList}
            </ScrollView>
          </AtTabsPane>
        </AtTabs>

        {/*海报生成器*/}
        <poster
          id='poster'
          config={posterConfig}
          onSuccess={this.onPosterSuccess.bind(this, 'share')}
          onFail={this.onPosterFail.bind(this)}
        />

        {/*全屏海报生成器*/}
        <poster
          id='poster2'
          config={posterConfig2}
          onSuccess={this.onPosterSuccess.bind(this, 'poster')}
          onFail={this.onPosterFail.bind(this)}
        />

        {/*actionsheet*/}
        <AtActionSheet
          isOpened={shareActionSheetOpen}
          cancelText='取消'
          onCancel={this.handleActionSheetCancel.bind(this)}
          onClose={this.handleActionSheetClose.bind(this)}
        >
          <AtActionSheetItem onClick={this.handleActionSheetClick.bind(this, 'share')}>
            <AtButton openType='share' className='share-action-btn'>
              <Text className='text'>分享</Text>
            </AtButton>
          </AtActionSheetItem>
          <AtActionSheetItem onClick={this.handleActionSheetClick.bind(this, 'poster')}>
            <AtButton className='share-action-btn'>
              <Text className='text'>生成卡片保存分享</Text>
            </AtButton>
          </AtActionSheetItem>
        </AtActionSheet>

        {/*二维码*/}
        {show_qrcode && <View className='qrcode_model' onClick={this.onStopClick} onTouchMove={this.onStopClick}>
          <Canvas className='scanCode' canvasId='myQrcode' />
          <AtButton type='primary'  className='qrcode_model_btn'  size='normal' onClick={this.onCloseQR.bind(this)}>关闭</AtButton>
        </View>}
      </View>
    )
  }
}

export default Order
