import Taro, { Component } from '@tarojs/taro'
import {Image, Swiper, SwiperItem, View, Text, Button} from '@tarojs/components'
import { connect } from '@tarojs/redux'
import { AtAvatar, AtButton, AtIcon, AtActionSheet, AtActionSheetItem, AtModal, AtModalHeader, AtModalContent, AtModalAction }  from 'taro-ui'
import "taro-ui/dist/weapp/components/countdown/index.scss";

import { asyncGoodDetailInfo, setGoodBasic, asyncGoodGroup, setGoodGroup } from '../../actions/good'
import { asyncSmallCodeDetail } from '../../api/order'
import WxParse from '../../components/wxParse/wxParse'

import './index.styl'
import NumberInput  from '../../components/NumberInput/index'
import {asyncJoinGroup, asyncCancelGroup} from '../../api/group'
import {jumpUrl} from "../../utils/router";
import withShare from '../../components/withShare'

import Poster from '../../components/poster/poster/poster'
import Poster2 from '../../components/poster/poster/posterfull'
import defaultShareImg from '../../assets/images/share.jpg';
import shareCodeImg from '../../assets/images/code.jpeg';
import pinIcon from '../../assets/images/pinicon.png'
import locationImg from '../../assets/images/pin.png'
import shopImg from '../../assets/images/shop.png'
import homeImg from '../../assets/images/home.png'
import shareCardBg from '../../assets/images/sharecardbg.png'

@withShare({})
@connect(({ good, user, shop }) => ({
  good, user, shop
}), (dispatch) => ({
  // 设置商品信息
  onPageInit (data) {
    dispatch(setGoodBasic(data))
  },
  // 加载商品详情
  onAsyncDetail () {
    dispatch(asyncGoodDetailInfo())
  },
  // 加载拼团信息
  onLoadGroup () {
    dispatch(asyncGoodGroup())
  },
  // 更新倒计时
  onSetGoodGroup (data) {
    dispatch(setGoodGroup(data))
  }
}))
class Good extends Component {
  constructor () {
    super(...arguments)
    this.state = {
      smallCodeImg: '',
      num: 1,
      posterConfig : {
        width: 1000,
        height: 800,
        backgroundColor: '#fff',
        debug: false,
        images: [
          {
            url: defaultShareImg,
            width: 800,
            height: 800,
            y: 0,
            x: 100,
          }
        ]
      },
      posterConfig2: {
        width: 750,
        height: 1334,
        backgroundColor: '#fff',
        debug: false,
        texts: [
          // 名称
          {
            x: 750 / 2,
            y: 750 * 1.1,
            text: `社区生活家`,
            fontSize: Math.floor(750 * 0.06),
            color: '#555',
            opacity: 1,
            zIndex: 9,
            textAlign: 'center',
            width: 650
          },
          // 价格
          {
            x: 750 / 2,
            y: 750 * 1.17,
            text: `新鲜低价`,
            fontSize:  Math.floor(750 * 0.05),
            color: '#FD7474',
            opacity: 1,
            zIndex: 9,
            textAlign: 'center',
            width: 650
          },
          // 描述
          {
            x: 750 / 2,
            y: 750 * 1.24,
            text: `长按识别小程序二维码`,
            fontSize: Math.floor(750 * 0.04),
            color: '#B2B2B2',
            opacity: 1,
            zIndex: 9,
            textAlign: 'center',
            width: 650
          },
        ],
        images: [
          {
            url:  defaultShareImg,
            width: 750,
            height: 750,
            y: 0,
            x: 0,
          },
          {
            url: shareCodeImg,
            width: 750 * .4,
            height: 750 * .4,
            y: 750 * 1.3,
            x: Math.floor(750 * .3),
          }
        ]
      },
      shareActionSheetOpen: false, // 打开分享弹窗
      shareImg: defaultShareImg,
      groupItem: {}, // 要参的团
      groupModalShow: false // 展示参团弹窗
    }
  }
  config = {
    navigationBarTitleText: '商品详情',
    usingComponents: {
      'poster': '../../components/poster/poster/index',
      'count-down': '../../components/count-down/count-down'
    }
  }
  // 计数器的数量变化
  handleNumChange (value) {
    this.setState({
      num: value
    })
  }

  // 参加拼团
  joinGroup (groupItem) {
    this.setState({
      groupItem,
      groupModalShow: true
    })
  }

  // 取消参团
  cancelGroupModal () {
    this.setState({
      groupItem: {},
      groupModalShow: false
    })
  }

  // 确认参团
  /**
   * @param keep {boolean} 保留原有
   * */
  confirmGroupModal (keep) {
    const {groupItem} = this.state
    const {token} = this.props.user
    const { group, id } = this.props.good
    Taro.showLoading({})
    asyncJoinGroup({
      token,
      product_id: id,
      product_group_id: group.id,
      parent_id: groupItem.id,
      had_ok: keep
    }).then(({status, msg, data}) => {
      Taro.hideLoading()
      Taro.showToast({
        title:  '参团成功' ,
        icon: 'none'
      })
      this.setState({
        groupModalShow: false
      })
      if (status) {
        this.goGroup()
      } else if (status == false && data.had_group) {
          // 不成功
          Taro.showModal({
            title: '提示',
            content:msg,
            cancelText: '不保留',
            confirmText: '保留'
          })
            .then(res => {
              console.log(res.confirm, res.cancel)
              if (res.confirm) {
                this.confirmGroupModal(true)
              }
              if (res.cancel) {
                asyncCancelGroup(token, data.group_code).then(() => {
                  this.confirmGroupModal(false)
                })
              }
            })
      } else {
        Taro.showToast({
          title: msg,
          icon: 'none'
        })
      }
    }).catch(() => {
      Taro.hideLoading()
      this.setState({
        groupModalShow: false
      })
    })
  }

  // 创建拼团
  createGroup () {
    const {token} = this.props.user
    const { group, id } = this.props.good

    asyncJoinGroup({
      token,
      product_id: id,
      product_group_id: group.id
    }).then(({status, msg}) => {
      Taro.showToast({
        title: status ? '参团成功' : msg,
        icon: 'none'
      })
      // 创建拼团成功
      if (status) {
        this.goGroup()
      }
    })
  }
  // 下拉刷新
  onPullDownRefresh () {
    Taro.stopPullDownRefresh()
  }
  // 跳转
  jumpTo (url) {
    jumpUrl(url, {method: 'navigateTo'})
  }
  // 回首页
  goHome () {
    // const from = this.$router.params.from
    // Taro.navigateBack({ delta: from === 'home' ? 1 : 2 })
    Taro.switchTab({url: '/pages/index/index'})
  }

  //直接购买
  goSettle () {
    const {num} = this.state
    this.jumpTo(`/pages/cart/settle?type=3&num=${num}`)
  }
  // 拼购购买
  goGroup () {
    const {num} = this.state
    this.jumpTo(`/pages/cart/settle?type=2&num=${num}`)
  }

  onPosterSuccess (preview, e) {
    const { detail } = e;
    // 预览
    if (preview === 'poster') {
      Taro.previewImage({
        current: detail,
        urls: [detail]
      })
    }
    // 分享
    if (preview === 'share') {
      this.setState({
        shareImg: detail
      })
    }
  }

  onPosterFail () {
    console.log('失败')
  }

  // 设置分享标题
  $setShareTitle = () => {
    const {name} = this.props.good
    return `${name}`
  }

  $setShareImageUrl = () => {
    console.log('分享图片', this.state.shareImg)
    return this.state.shareImg
  }

  componentWillReceiveProps (nextProps) {
    const { detail, thumb,   group, weight, spec_name,sale_price } = nextProps.good
    WxParse.wxParse('detail', 'html', detail, this.$scope, 5)
    this.setState({
      posterConfig: {
        width: 1000,
        height: 800,
        paddingLeft:80,
        backgroundColor: '#fff',
        debug: false,
        blocks: [
          {
            x: 0,
            y: 600,
            width: 1000, // 如果内部有文字，由文字宽度和内边距决定
            height: 200,
            paddingLeft: 0,
            paddingRight: 0,
            borderWidth: 0,
            backgroundColor: '#FFC93E',
            borderRadius: 40,
            zIndex: 1,
          },
          {
            x: 0,
            y: 760,
            width: 40,
            height: 40,
            paddingLeft: 0,
            paddingRight: 0,
            borderWidth: 0,
            backgroundColor: '#FFC93E',
            borderRadius: 0
          },
          {
            x: 960,
            y: 760,
            width: 40,
            height: 40,
            paddingLeft: 0,
            paddingRight: 0,
            borderWidth: 0,
            backgroundColor: '#FFC93E',
            borderRadius: 0
          }
        ],
        texts: [
          {
            x: 50,
            y: 660,
            text: [
              {
                text: `¥ ${parseFloat(group ? (group.group_price / 100) : 0).toFixed(2)}`,
                fontSize: 80,
                color: 'white',
                opacity: 1,
                marginLeft: 0,
                marginRight: 40,
                lineHeight: 140,
                lineNum: 1,
              },
              {
                text: `¥${parseFloat(sale_price / 100).toFixed(2)}`,
                fontSize: 56,
                color: '#B2B2B2',
                marginLeft: 20,
                textDecoration: 'line-through'
              }
            ],
           
            zIndex: 9,
            baseLine: 'middle',
          },
          {
            x: 50,
            y: 740,
            text: `${weight}g/${spec_name}`,
            fontSize: 50,
            color: 'white',
            opacity: 1,
            baseLine: 'middle',
            zIndex: 9,
          },
        ],
        images: [
          {
            url: thumb || defaultShareImg,
            width: 800,
            height: 800,
            y: 0,
            x: 100,
          },
          {// 拼图标
            url: pinIcon,
            width: 120,
            height: 120,
            y: 20,
            x:20,
            zIndex: 9,
          }
        ]
      }
    })
    setTimeout(() => {
      Poster.create(true)    // 入参：true为抹掉重新生成
    }, 200)
    // 倒计时定时器
    // if (this.props.good.group_list_id !== group_list_id) {
    //   if (this.interval) clearInterval(this.interval)
    //   this.interval = setTimeout(() => {
    //     this.props.onSetGoodGroup({
    //       group_list: group_list.map(group => {
    //         return Object.assign({}, group, {minutes: group.minutes - 1})
    //       })
    //     })
    //   }, 1000 * 60)
    // }
  }

  handleActionSheetOpen () {
    this.setState({
      shareActionSheetOpen: true
    })
    console.log("获取id")
    console.log(this)
    const {  goodId } = this.props.good
    let scene_value =goodId
    const {origin} = this.props.user
    asyncSmallCodeDetail( scene_value)
        .then(({status, data}) => {
          console.log(status)
          this.setState({
            smallCodeImg : origin +"/"+ data
          })
        })
        // .catch(() => {
        //   Taro.showToast({
        //     title: '操作失败',
        //     icon: 'none'
        //   })
        // })
  }
  // 分享actionsheet 关闭
  handleActionSheetClose () {
    this.setState({
      shareActionSheetOpen: false
    })
  }

  // 分享actionsheet 点击
  handleActionSheetClick (type) {
    this.setState({
      shareActionSheetOpen: false
    })
    if (type === 'poster') {
      const {  name, spec_name, thumb, price, sales, tags, group, weight } = this.props.good
      const {avatar, nickname} = this.props.user
      const {name: shopname, address } = this.props.shop
      Taro.getSystemInfo({}).then(({screenWidth, screenHeight, pixelRatio}) => {
        console.log(screenWidth, screenHeight, pixelRatio)
        const width = screenWidth * pixelRatio
        const height = Math.max(screenHeight * pixelRatio, Math.floor(width * 2.1))
        const codeWidth = Math.floor(width * .4)
        const topheight = 150
        const infoblockH = width * 0.6 + 390 + topheight
        const textxLeft = 40
        let tags_width = 0
        this.setState({
          posterConfig2: {
            width,
            height:height - 50,
            y: topheight,
            backgroundColor: '#000',
            debug: false,
            blocks: [{
              // 商品规格
              x: width - 230,
              y: width * 0.6 + 220 + topheight,
              width: 200,
              height: Math.floor(width * 0.04) * 1.5,
              text: {
                text: `${weight}g/${spec_name}`,
                fontSize: Math.floor(width * 0.04),
                lineHeight: Math.floor(width * 0.04) * 1.5,
                color: '#0f59a4',
                opacity: 1,
                zIndex: 9,
                textAlign: 'right',
              }
            },
              // tags
              ...tags.map((tag, index) => {
                const  fontSize = Math.floor(width * 0.03)
                const tag_width = fontSize * tag.name.length * 1.1 + 40
                const tag_height = fontSize * 1.5
                tags_width = tags_width + tag_width + 30
                return {
                  x: tags_width - tag_width,
                  y: width * 0.6 + 280 + topheight,
                  width: tag_width,
                  height: tag_height,
                  backgroundColor: 'transparent',
                  borderRadius: 10,
                  borderWidth:2,
                  borderStyle:'solid',
                  borderColor:'#FFD701',
                  zIndex: 9,
                  text: {
                    text: `${tag.name}`,
                    fontSize: fontSize,
                    lineHeight: fontSize * 1.5,
                    color: '#333',
                    opacity: 1,
                    textAlign: 'center',
                    baseLine: 'middle'
                  }
                }
              }),
              // 分界线
              {
                  width: width * 0.96,
                  height: 1,
                  x: width * 0.02,
                  y: width * 0.6 + 360 + topheight,
                  backgroundColor: '#C7C7C7',
              },
            ],
            texts: [
              {
                // 微信昵称
                x: 130,
                y: 70 + topheight,
                text: nickname,
                fontSize: Math.floor(width * 0.05),
                lineHeight: 80,
                baseLine: 'middle',
                color: '#555',
                opacity: 1,
                zIndex: 9,
                textAlign: 'left',
                width: width - 160
              },
              // 商品名称
              {
                x: textxLeft,
                y: width * 0.6 + 230 + topheight,
                text: `${name}`,
                fontSize: Math.floor(width * 0.05),
                color: '#555',
                opacity: 1,
                zIndex: 9,
                textAlign: 'left',
                width: width - 260
              },
              // 人数
              {
                x: textxLeft +20,
                y: infoblockH + 38,
                baseLine: 'middle',
                text: [
                  {
                    text: `${group.group_num}人`,
                    fontSize: Math.floor(width * 0.06),
                    color: '#ec1731',
                    marginLeft: Math.floor(width * 0.08) +20,
                  }
                ]
              },
              //价格
                {
                    x: width - 390,
                    y: infoblockH + 38,
                    baseLine: 'middle',
                    textAlign: 'left',
                    text: [
                        {
                            text: `¥${parseFloat(group ? (group.group_price / 100) : 0).toFixed(2)}`,
                            fontSize: Math.floor(width * 0.06),
                            color: '#ec1731',
                            marginLeft: 0,
                        },
                        {
                            text: `¥${parseFloat(price / 100).toFixed(2)}`,
                            fontSize: Math.floor(width * 0.04),
                            color: '#B2B2B2',
                            marginLeft: 20,
                            textDecoration: 'line-through'
                        }
                    ]
                },
              // 描述
              {
                x: width / 2,
                y: width * 2 + topheight,
                text: `长按识别小程序二维码`,
                fontSize: Math.floor(width * 0.04),
                color: '#B2B2B2',
                opacity: 1,
                zIndex: 9,
                textAlign: 'center',
                width: width - 100
              },
              // 店铺名称
              {
                x: 50 + Math.floor(width * 0.06),
                y: width * 1.29 + topheight,
                text: `${shopname}`,
                fontSize: Math.floor(width * 0.04),
                color: '#515151',
                opacity: 1,
                zIndex: 9,
                textAlign: 'left',
                baseLine: 'bottom',
                width: width - 80 - Math.floor(width * 0.05)
              },
              // 店铺地址
              {
                x: 50 + Math.floor(width * 0.06),
                y: width * 1.39 + topheight,
                lineHeight: Math.floor(width * 0.04)+10,
                text: `${address}`,
                fontSize: Math.floor(width * 0.04),
                height:Math.floor(width * 0.04),
                color: '#515151',
                opacity: 1,
                zIndex: 9,
                textAlign: 'left',
                baseLine: 'bottom',
                width: width - 80 - Math.floor(width * 0.05)
              },
            ],
            images: [
              { // 背景图片
                url: shareCardBg,
                width: width,
                height: height - 50,
                position: 'absolute',
                y: topheight,
                x: 0,
                zIndex: 0,

              },
              { // 商品大图
                  url: thumb || defaultShareImg,
                  width: width * 0.8,
                  height: width * 0.6 ,
                  y: 150 + topheight,
                  x: width * 0.1,
              },
              { // 用户头像
                url: avatar ,
                width: 80,
                height: 80,
                y: 30 + topheight,
                x: textxLeft,
                borderRadius: 80,
                // borderWidth: 1
              },
              { // shop图标
                url: shopImg,
                width: Math.floor(width * 0.05),
                height: Math.floor(width * 0.05),
                y: width * 1.24 + topheight,
                x: textxLeft
              },
              { // 位置 图标
                  url: locationImg,
                  width: Math.floor(width * 0.05),
                  height: Math.floor(width * 0.05),
                  y: width * 1.34 + topheight,
                  x: textxLeft
              },
              { // 拼图标
                url: pinIcon,
                width: Math.floor(width * 0.08),
                height: Math.floor(width * 0.08),
                y: infoblockH,
                x: textxLeft
              },
              { // 二维码图
                // url: shareCodeImg,
                url: this.state.smallCodeImg,
                width: codeWidth,
                height: codeWidth,
                y: width * 1.5 + topheight,
                x: Math.floor(width * .3),
              }
            ]
          }
        })
        setTimeout(() => {
          Poster2.create(true)    // 入参：true为抹掉重新生成
        }, 200)
      })
    }
  }

  componentDidMount () {
  
    const idvalueA =   this.$router.params.id
    const idvalueB =   this.$router.params.scene
    let goodId = ""
    if(idvalueB!=undefined){
      goodId = idvalueB
    }else{
      goodId = idvalueA 
    }
    //  const goodId =  this.$router.params.id
     console.log(goodId)
    this.props.onPageInit({goodId}) // 设置id
    this.props.onAsyncDetail() // 加载商品详情
    this.props.onLoadGroup() // 加载拼团信息
  }
  componentWillUnmount () {
    clearInterval(this.interval)
  }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    const { banner, name, thumb, sales, describe, price, group_total, group, group_list, tags, spec_name, weight, sale_price, group_price } = this.props.good
    const { num, posterConfig, posterConfig2, shareActionSheetOpen, groupItem, groupModalShow } = this.state
    const group_list2 = []
    console.info(this.props.good.group_list);
    group_list.map((g, index) => {
      const i = Math.floor(index / 2)
      if (!group_list2[i]) {
        group_list2[i] = []
      }
      group_list2[i].push(g)
    })
    let bannerSwiper = banner.map((item, index) => {
      return (<SwiperItem key={index}>
        <View className='good-banner-item'>
          <Image src={item} className='good-banner-img' />
        </View>
      </SwiperItem>
      )
    })
    // 参团 的团轮播
    const groupSwiper = group_list2.map((item, index) => {
      const item1 = item[1]
      return (<SwiperItem key={index}>
          <View className='group-swiper-item'>
            <AtAvatar circle image={item[0].user.avatar} className='group-swiper-avatar' />
            <Text className='group-swiper-username'>{item[0].user.nickname}</Text>
            <View className='group-swiper-time'>
              { (item[0].num < group.group_num) && <View className='group-swiper-left'>还差 <Text className='group-swiper-num'>{group.group_num - item[0].num}</Text> 人拼成</View> }
              { (item[0].num >= group.group_num) && <View className='group-swiper-left'>您可以直接参团</View> }
              <count-down initDuration={item[0].minutes * 60} />
            </View>
            <AtButton type='primary' className='group-swiper-btn' onClick={this.joinGroup.bind(this, item[0])}>去拼单</AtButton>
          </View>
        {item1 && <View className='group-swiper-item'>
          <AtAvatar circle image={item1.user.avatar} className='group-swiper-avatar' />
          <Text className='group-swiper-username'>{item1.user.nickname}</Text>
          <View className='group-swiper-time'>
            { (item1.num < group.group_num) && <View className='group-swiper-left'>还差 <Text className='group-swiper-num'>{group.group_num - item1.num}</Text> 人拼成</View>}
            { (item1.num >= group.group_num) && <View className='group-swiper-left'>您可以直接参团</View> }
            <count-down initDuration={item[1].minutes * 60} />
          </View>
          <AtButton type='primary' className='group-swiper-btn' onClick={this.joinGroup.bind(this, item1)}>去拼单</AtButton>
          </View>}
        </SwiperItem>
      )
    })

    const renderTags = tags.map((tag, index) => {
      return (<View key={index} className='good-tags-item'>{tag.name}</View>)
    })

    return (
      <View className='good'>
        {/* 商品轮播 */}
        <Swiper
          className='good-swiper'
          indicatorColor='#999'
          indicatorActiveColor='#333'
          circular
          indicatorDots
          autoplay
        >
          {banner.length > 0 && bannerSwiper}
          {banner.length === 0 && <SwiperItem>
            <View className='good-banner-item'>
              <Image src={thumb} className='good-banner-img' />
            </View>
          </SwiperItem>}
        </Swiper>
        {/* 头部商品信息*/}
        <View className='good-header'>
          {/*分享*/}
          {/*<Button openType='share' className='share'>*/}
          <Button className='share' onClick={this.handleActionSheetOpen.bind(this)}>
            <AtIcon value='external-link' size='18' color='#FFF' />
            <Text className='text'>分享</Text>
          </Button>
          <View className='good-header-top'>
            <Text className='good-name'>{name}</Text>
            <Text className='good-sales'>{weight} g / {spec_name}</Text>
          </View>
          <View className='good-describe'>{describe}</View>
          <View className='good-price'>
            <Text className='price'>{parseFloat(group_price / 100).toFixed(2)}</Text>
            <View className='num'>
              <NumberInput
                min={1}
                max={100}
                step={1}
                width={200}
                value={num}
                onChange={this.handleNumChange.bind(this)}
              />
            </View>
          </View>
        </View>
        {/*富文本商品内容*/}
        <View className='good-richText'>
          <import src='../../components/wxParse/wxParse.wxml' />
          <template is='wxParse' data='{{wxParseData:detail.nodes}}'/>
        </View>
        {/*拼购团信息*/}
        <View className='good-group'>
          <View className='good-group-header'>
            已有{group_total}人参与拼团，您可直接参与
          </View>
          <Swiper
            className='group-swiper'
            vertical
            autoplay
          >
            {groupSwiper}
          </Swiper>
        </View>
        {/*标签信息*/}
        <View className='good-tags'>
          <View className='good-tags-header'>
            标签
          </View>
          <View className='good-tags-list'>
            {renderTags}
          </View>
        </View>
         {/*商品详情*/}
          <View className='good-details'>
            <View className='good-details-header'>
              商品详情
            </View>
           
            <View className='good-details-list'>规格：{weight} g / {spec_name}</View>
          </View>
        {/*海报生成器*/}
        <poster
          id='poster'
          config={posterConfig}
          onSuccess={this.onPosterSuccess.bind(this, 'share')}
          onFail={this.onPosterFail.bind(this)}
          style='margin-top:160px;background:transparent'
        />

        {/*全屏海报生成器*/}
        <poster
          id='poster2'
          config={posterConfig2}
          onSuccess={this.onPosterSuccess.bind(this, 'poster')}
          onFail={this.onPosterFail.bind(this)}
          style='margin-top:160px;background:transparent'
        />

        {/*底部*/}
        <View className='good-bottom'>
          <View className='good-bottom-home' onClick={this.goHome.bind(this)}>
            <Image src={homeImg}  style='width: 20px;height: 20px;' />
            <View>首页</View>
          </View>
          <View className='good-bottom-cart' onClick={this.goSettle.bind(this)}>
            <View className='price'>{parseFloat(sale_price / 100).toFixed(2)}</View>
            <View>单独购买</View>
          </View>
          { group && <View className='good-bottom-group' onClick={this.createGroup.bind(this, null)}>
            <View className='price'>{parseFloat(group.group_price / 100).toFixed(2)}</View>
            <View>发起拼团</View>
          </View>}
        </View>

        {/*actionsheet*/}
        <AtActionSheet
          isOpened={shareActionSheetOpen}
          cancelText='取消'
          onCancel={this.handleActionSheetClose.bind(this)}
          onClose={this.handleActionSheetClose.bind(this)}
        >
          <AtActionSheetItem onClick={this.handleActionSheetClick.bind(this, 'share')}>
            <AtButton openType='share' className='share-action-btn'>
              <Text className='text'>分享</Text>
            </AtButton>
          </AtActionSheetItem>
          <AtActionSheetItem onClick={this.handleActionSheetClick.bind(this, 'poster')}>
            <AtButton className='share-action-btn'>
              <Text className='text'>生成卡片保存分享</Text>
            </AtButton>
          </AtActionSheetItem>
        </AtActionSheet>

        {/*参团弹窗*/}
        <AtModal isOpened={groupModalShow} className='group-modal'>
          <AtModalHeader>参与{groupItem.user.nickname}的拼单</AtModalHeader>
          <AtModalContent>
            <View className='group-modal-text'>
              <Text>还差</Text> <Text className='group-modal-num'>{group.group_num - groupItem.num}</Text><Text>人拼成</Text>
              <Text>，</Text>
              <View className='group-modal-inline' ><count-down  initDuration={groupItem.minutes * 60} /></View> <Text>后结束</Text>
            </View>
            {/*头像*/}
            <View className='group-modal-avatars'>
              <AtAvatar circle size='small' image={groupItem.user.avatar} className='group-modal-avatar' />
              <AtAvatar circle size='small' text='?' className='group-modal-avatar' />
            </View>

          </AtModalContent>
          <AtModalAction>
            <Button onClick={this.cancelGroupModal.bind(this)}>取消</Button>
            <Button onClick={this.confirmGroupModal.bind(this, false)} className='group-modal-btn'>参与拼团</Button> </AtModalAction>
        </AtModal>

        {/*重复团提示*/}
      </View>
    )
  }
}

export default Good
