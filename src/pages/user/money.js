import Taro, { Component } from '@tarojs/taro'
import {View, Text, ScrollView} from '@tarojs/components'
import {AtCard, AtTabsPane} from 'taro-ui'

import { connect } from '@tarojs/redux'
import { jumpUrl } from '../../utils/router'
import {asyncUserMoney, resetMoneyLog, asyncUserMoneyLog} from '../../actions/user'
import './money.styl'

@connect(({ daily, groupBuy, shop, user }) => ({
  daily,
  shop,
  groupBuy,
  user
}), (dispatch) => ({
  // 加载用户余额
  onAsyncUserMoney () {
    dispatch(asyncUserMoney())
  },
  // 重置历史记录
  onResetMoneyLog () {
    dispatch(resetMoneyLog())
  },
  // 加载历史记录
  onAsyncUserMoneyLog () {
    dispatch(asyncUserMoneyLog())
  },
}))
class Center extends Component {
  constructor () {
    super(...arguments)
    this.state = {}
  }

  config = {
    navigationBarTitleText: '我的余额',
    onReachBottomDistance: 300
  }

  // 页面到达底部
  onReachBottom () {
    this.props.onAsyncUserMoneyLog()
  }

  // 下拉刷新
  onPullDownRefresh () {
    Taro.stopPullDownRefresh()
  }

  onGetUserInfo (data) {
    console.log(data)
  }

  handleOrderClick (value) {
    console.log(value)
  }
  // 跳转
  jumpTo (url) {
    jumpUrl(url, {method: 'navigateTo'})
  }

  componentWillReceiveProps (nextProps) {
    console.log(this.props, nextProps)
  }
  componentDidMount () { }
  componentWillUnmount () { }

  componentDidShow () {
    // 获取用户余额
    this.props.onAsyncUserMoney()
    this.props.onResetMoneyLog()
    this.props.onAsyncUserMoneyLog()
  }

  componentDidHide () { }

  render () {
    const {money, money_history} = this.props.user

    const renderLogList = money_history.map((order, index) => {
      return (<View key={index} className='order-item'>
        <Text className='order-item__date'>{order.created_at}</Text>
        <Text>{order.type_txt}</Text>
        <Text>{order.money_txt} 元</Text>
      </View>)
    })

    return (
      <View className='user-money'>
        <AtCard
          note='余额来自购物返现，红包等途径，您可以在购物时直接使用'
          extra={'￥' + parseFloat(money === null ? 0 : (money / 100)).toFixed(2) + '元'}
          title='我的余额'
        >
        </AtCard>
        {money_history.length > 0 && <ScrollView className='order-list'>
          {renderLogList}
        </ScrollView>}
        {money_history.length === 0 && <View className='no'>暂无记录</View>}
      </View>
    )
  }
}

export default Center
