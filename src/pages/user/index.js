import Taro, { Component } from '@tarojs/taro'
import {View, Text} from '@tarojs/components'
import { AtAvatar, AtButton, AtTabBar, AtList, AtListItem, AtIcon } from 'taro-ui'

import { connect } from '@tarojs/redux'
import { jumpUrl } from '../../utils/router'
import { asyncOrderCount } from '../../api/order'
import {asyncUserMoney} from '../../actions/user'
import './index.styl'

@connect(({ daily, groupBuy, shop, user }) => ({
  daily,
  shop,
  groupBuy,
  user
}), (dispatch) => ({
  // 加载用户余额
  onAsyncUserMoney () {
    dispatch(asyncUserMoney())
  }
}))
class Center extends Component {
  constructor () {
    super(...arguments)
    this.state = {
      wait_pay: 0,
      wait_point: 0,
      wait_receive: 0
    }
  }
  config = {
    navigationBarTitleText: '个人中心'
  }

  // 下拉刷新
  onPullDownRefresh () {
    Taro.stopPullDownRefresh()
  }

  onGetUserInfo (data) {
    console.log(data)
  }

  handleOrderClick (value) {
    this.jumpTo('/pages/order/index?from=center&tab=' + value)
  }
  // 跳转
  jumpTo (url) {
    jumpUrl(url, {method: 'navigateTo'})
  }

  componentWillReceiveProps (nextProps) {
    console.log(this.props, nextProps)
  }
  componentDidMount () {
    // 查看用户已授权信息
    Taro.getSetting({}).then(data => {
      console.log('用户已授权信息')
      console.log(data)
    }).catch(err => console.log(err))
  }
  componentWillUnmount () { }

  componentDidShow () {
    const {token} = this.props.user
    asyncOrderCount(token).then(({data, status}) => {
      if (status) {
        const { wait_pay, wait_point, wait_receive } = data
        this.setState({
          wait_pay,
          wait_point,
          wait_receive
        })
      }
    })
    // 获取用户余额
    this.props.onAsyncUserMoney()
  }

  componentDidHide () { }

  render () {
    const {
      wait_pay,
      wait_point,
      wait_receive
    } = this.state
    const {money} = this.props.user
    console.log(money)
    return (
      <View className='center'>
        {/* 黄色背景的头部 */}
        <View className='header'>
          {/* TODO wx.authorize({scope: "scope.userInfo"})，无法弹出授权窗口，请使用 <button open-type="getUserInfo"/>*/}
          {/*<Button className='btn-max-w' plain type='primary' open-type='getUserInfo' lang='zh_CN' onGetUserInfo={this.onGetUserInfo.bind(this)}>公开信息</Button>*/}
          <AtAvatar openData={{ type: 'userAvatarUrl'}} circle className='user-avatar' />
          <open-data type='userNickName' lang='zh_CN' className='user-nick' />
          {/*<AtButton type='secondary' circle className='privilege-btn'> <Text>我的特权</Text><Text className='shopfont shop-arrow' /></AtButton>*/}
        </View>
        {/* 我的订单 */}
        <View className='order-section'>
          <View className='order-section-header'>
            <Text className='title'>我的订单</Text>
            <Text className='all' onClick={this.jumpTo.bind(this, '/pages/order/index')}>查看全部订单</Text>
          </View>
          <AtTabBar
            color='#111'
            className='order-bar'
            selectedColor='#111'
            fontSize='10'
            tabList={[
              // { title: '待确认', iconPrefixClass:'order', iconType: 'unpay', text: wait_pay > 0 ? String(wait_pay) : '', max: '99' },
              // { title: '待收货', iconPrefixClass:'order', iconType: 'unreceipt', text: wait_receive > 0 ? String(wait_receive) : '', max: '99' },
              { title: '全部', iconPrefixClass:'order', iconType: 'finished' },
              { title: '待评价', iconPrefixClass:'order', iconType: 'uncomment', text:  wait_point > 0 ? String(wait_point) : '', max: '99' }
            ]}
            onClick={this.handleOrderClick.bind(this)}
          />
        </View>
        {/*router块*/}
        <View className='router-section'>
          <View className='router-item' onClick={this.jumpTo.bind(this, '/pages/address/index')}>
            <Text className='label'>管理收货地址</Text>
            <AtIcon value='chevron-right' size='16' color='#333333' />
          </View>
        </View>
        {/*router块*/}
        <View className='router-section'>
          <View className='router-item'  onClick={this.jumpTo.bind(this, '/pages/voucher/index')}>
            <Text className='label'>我的代金券</Text>
            <AtIcon value='chevron-right' size='16' color='#333333' />
          </View>
        </View>

        {/*router块*/}
        <View className='router-section'>
          <View className='router-item'  onClick={this.jumpTo.bind(this, '/pages/user/money')}>
            <Text className='label'>我的余额</Text>
            <View className='right'>
              {money !== null && <Text className='add-info'>￥{parseFloat( money / 100 ).toFixed(2)}元</Text>}
              <AtIcon value='chevron-right' size='16' color='#333333' />
            </View>
          </View>
        </View>
      </View>
    )
  }
}

export default Center
