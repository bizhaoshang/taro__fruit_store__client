import Taro, { Component } from '@tarojs/taro'
import {View, Text} from '@tarojs/components'
import { AtCard } from 'taro-ui'

import { connect } from '@tarojs/redux'
import { jumpUrl } from '../../utils/router'
import {asyncUserMoney} from '../../actions/user'
import './money.styl'

@connect(({ daily, groupBuy, shop, user }) => ({
  daily,
  shop,
  groupBuy,
  user
}), (dispatch) => ({
  // 加载用户余额
  onAsyncUserMoney () {
    dispatch(asyncUserMoney())
  }
}))
class Privilege extends Component {
  constructor () {
    super(...arguments)
    this.state = {}
  }
  config = {
    navigationBarTitleText: '我的余额'
  }

  onGetUserInfo (data) {
    console.log(data)
  }

  // 下拉刷新
  onPullDownRefresh () {
    Taro.stopPullDownRefresh()
  }

  handleOrderClick (value) {
    console.log(value)
  }
  // 跳转
  jumpTo (url) {
    jumpUrl(url, {method: 'navigateTo'})
  }

  componentWillReceiveProps (nextProps) {
    console.log(this.props, nextProps)
  }
  componentDidMount () { }
  componentWillUnmount () { }

  componentDidShow () {
    // 获取用户余额
    this.props.onAsyncUserMoney()
  }

  componentDidHide () { }

  render () {
    const {money} = this.props.user
    return (
      <View className='user-money'>
        <AtCard
          note='余额来自购物返现，红包等途径，您可以在购物时直接使用'
          extra={'￥' + parseFloat(money === null ? 0 : (money / 100)).toFixed(2) + '元'}
          title='我的余额'
        >
        </AtCard>
      </View>
    )
  }
}

export default Privilege
